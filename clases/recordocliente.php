<?php
    require_once"../conexion/conexion.php";

    class RecordCliente extends Conexion{

    	private $id_credito;
    	private $id_cliente;
    	

    public function RecordCliente(){
            parent::__construct();
        }	
        
    public function getIdCredito()
    {
        return $this->id_credito;
    }

    public function setIdCredito($id_credito)
    {
        $this->id_credito = $id_credito;

        return $this;
    }

    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;

        return $this;
    }



         public function consultar_record(){
            $sql="call consultar_record(?)";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->bindParam(1,$this->id_cliente,PDO::PARAM_STR);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

                $sentencia->closeCursor();

                return $resultado;

                $this->conexionDB=null;            
     
        }


   
} 