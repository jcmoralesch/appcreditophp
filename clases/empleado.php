<?php
    require"../conexion/conexion.php";

    class Empleado extends Conexion{

    	private $identificacion;
    	private $nombre;
    	private $apellido;  
    	private $direccion;
    	private $cargo;   	
        private $ruta;
        private $telefono;

    	public function Empleado(){
    		parent::__construct();
    	}


    	public function setIdentificacion($identificacion){
    		$this->identificacion=$identificacion;
    	}

    	public function setNombre($nombre){
    		$this->nombre=$nombre;
    	}

    	public function setApellido($apellido){
    		$this->apellido=$apellido;
    	}
      
    	public function setDireccion($direccion){
    		$this->direccion=$direccion;
    	} 

    	public function setCargo($cargo){
    		$this->cargo=$cargo;
    	}

        public function setRuta($ruta){
            $this->ruta=$ruta;
        }

         public function setTelefono($telefono){
            $this->telefono=$telefono;
        }

    	public function getIdentficacion(){
    		return $this->identficacion;
    	}

    	public function getNombre(){
    		return $this->nombre;
    	}

    	public function getApellido(){
    		return $this->apellido;
    	}

    	public function getDireccion(){
    		return $this->direccion;
    	}        

    	public function geCargo(){
    		return $this->cargo;
    	}

        public function getRuta(){
            return $this->ruta;
        }

        public function getTelefono(){
            return $this->telefono;
        }

    	

    	public function registrar(){
    		try{
                $sql="call registrarEmpleado(:identificacion,:nombre,:apellido,:direccion,:cargo,:ruta,:telefono)";

                $datos=$this->conexionDB->prepare($sql);

        		$datos->bindParam(":identificacion",$this->identificacion,PDO::PARAM_STR);
        		$datos->bindParam(":nombre",$this->nombre,PDO::PARAM_STR);
        		$datos->bindParam(":apellido",$this->apellido,PDO::PARAM_STR);
          		$datos->bindParam(":direccion",$this->direccion,PDO::PARAM_STR);
        		$datos->bindParam(":cargo",$this->cargo,PDO::PARAM_STR);
                $datos->bindParam(":ruta",$this->ruta,PDO::PARAM_STR);
                $datos->bindParam(":telefono",$this->telefono,PDO::PARAM_STR);

        		$datos->execute();

        		header("Location:../formularios/empleado.php");
    		}
    		catch(Exception $e){
                echo"<html>
                           <head>
                               <link href='../estilomodal.css' rel='stylesheet'>
                               <link href='../Bootstrap/css/bootstrap.css' rel='stylesheet'>
                           </head>
                           <body>
                              <div id='ventanamodal'>
                                  <div class='alert alert-danger n' role='alert'>
                                 XXXX-ERROR TRANSACION NO REALIZADA  <a href='../formularios/empleado.php' class='alert-link'>  REGRESAR PAGINA PRINCIPAL</a>
                                  </div>
                               </div>
                           <body/>
                            </html>";       

    		}
    		finally{
                 $this->conexionDB=null;
    		}
    	}

        public function consultarUsuario(){

            $sql="select U.identificacion,E.nombre,E.apellido,E.id_ruta,U.usuario,U.privilegio from tabla_control U, empleado E 
                   where E.identificacion=U.identificacion";

            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function perfilemple($id){

            $sql="select * from datos_empleado where identificacion='".$id."'";

            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

         public function rutasemple(){

            $sql="select nombre,apellido, id_ruta from empleado";

            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function calcularRecaudo($fecha1,$fecha2,$ruta){
            $sql="select sum(D.cantidad) as cant,C.id_ruta,date_format(D.fecha_deposito,'%d/%m/%Y') as fech from deposito D, cliente C,credito R 
where D.id_credito=R.id_credito and C.identificacion=R.identificacion and C.id_ruta='".$ruta."' 
and D.fecha_deposito between '".$fecha1."' and '".$fecha2."' group by D.fecha_deposito";

            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function calcularPago($id){
             $sql="select S.id_pago,E.nombre,E.apellido,E.id_ruta,S.viaticos,S.sueldo_base,S.recaudo,S.porcentaje,S.sueldo_total,date_format(S.fecha_pago,'%d/%m/%Y') as fecha
from empleado E,pago_sueldo S where E.identificacion=S.identificacion and id_pago='".$id."'";

            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;

        }

    } 	

