<?php
    require"../conexion/conexion.php";

    class TipoCredito extends Conexion{

        private $fecha;

    	
    	public function TipoCredito(){
    		parent::__construct();
    	}


        public function getFecha()
        {
            return $this->fecha;
        }

        public function setFecha($fecha)
        {
            $this->fecha = $fecha;

            return $this;
        }

        public function consultarTCredito(){
            $sql="select * from tipo_credito";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


        public function consultarRuta(){
            $sql="select * from ruta";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function consultarTPago(){
            $sql="select * from  tipo_pago";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function consultarSueldo(){

            $sql="select * from sueldo";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


        public function generarCalendario($identificacion){

            $sql="select C.nombre,C.apellido,C.id_ruta,F.fecha from cliente C,fecha_pagos F, credito T 
where F.id_credito=T.id_credito and T.identificacion=C.identificacion and C.identificacion='".$identificacion."'";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function consultarRutaEmpleado($id){

            $sql="select id_ruta from empleado where identificacion='".$id."';";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function consultarRutaE($ruta,$ruta1){
            $pr=$ruta."%".$ruta1;

            $sql="select * from ruta where id_ruta like '$pr'";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


        public function depositos($fecha1,$fecha2){
            
            $sql="select distinct C.id_ruta,sum(D.cantidad),sum(D.mora),date_format(D.fecha_deposito,'%d/%m/%Y') from deposito D, cliente C, credito R  where 
                 D.id_credito=R.id_credito and C.identificacion=R.identificacion and fecha_deposito between '".$fecha1."' and '".$fecha2."' 
                  group by C.id_ruta,D.fecha_deposito order by D.fecha_deposito;";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function colocacion($fecha1,$fecha2){
            
            $sql="select C.id_ruta,sum(R.monto),count(R.monto),date_format(R.fecha_registro,'%d/%m/%Y') from credito R,cliente C where R.identificacion=C.identificacion and R.fecha_registro between
'".$fecha1."' and '".$fecha2."' group by C.id_ruta,R.fecha_registro order by R.fecha_registro";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


        public function contarClientes(){
            $sql="select count(*)+1 as numero from cliente";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


         public function modificarFecha(){
            try{
                    $datos=$this->conexionDB;
                    $datos->beginTransaction();

                    $sql="call actualizaFechaFestivo(:fecha)";

                    $sgb=$datos->prepare($sql);

                    $sgb->bindParam(":fecha",$this->fecha,PDO::PARAM_STR);
              
                    $sgb->execute();

                   
                    $datos->commit();
                   return true;

                }
                catch(Excetpion $e){
                
                        $datos->rollBack();
                     
                        return false;
                }
    }

    
}    





	

