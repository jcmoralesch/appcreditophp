<?php
    require"../conexion/conexion.php";

    class Sueldo extends Conexion{

    	private $sueldo_base;
    	private $depreciacion;
    	private $combustible;
        private $saldo;
    	private $porcentaje;
        private $tipo;

    	public function Sueldo(){
    		parent::__construct();
    	}


    	public function setSueldoBase($sueldo_base){
    		$this->sueldo_base=$sueldo_base;
    	}

    	public function setDepreciacion($depreciacion){
    		$this->depreciacion=$depreciacion;
    	}

    	public function setCombustible($combustible){
    		$this->combustible=$combustible;
    	}

        public function setSaldo($saldo){
            $this->saldo=$saldo;
        }

    	public function setPorcentaje($porcentaje){
    		$this->porcentaje=$porcentaje;
    	} 

    	public function setTipo($tipo){
    		$this->tipo=$tipo;
    	}

    	public function getSueldoBase(){
    		return $this->sueldo_base;
    	}

    	public function getDepreciacion(){
    		return $this->depreciacion;
    	}

    	public function getCombustible(){
    		return $this->combustible;
    	}

    	public function getSaldo(){
    		return $this->saldo;
    	}

        public function getPorcentaje(){
            return $this->porcentaje;
        }

    	public function getTipo(){
    		return $this->tipo;
    	}
    	

    	public function registrar(){
    		try{
                $sql="call registrarSueldo(:sueldo_base,:depreciacion,:combustible,:saldo,:porcentaje,:tipo)";

                $datos=$this->conexionDB->prepare($sql);

        		$datos->bindParam(":sueldo_base",$this->sueldo_base,PDO::PARAM_STR);
        		$datos->bindParam(":depreciacion",$this->depreciacion,PDO::PARAM_STR);
        		$datos->bindParam(":combustible",$this->combustible,PDO::PARAM_STR);
                $datos->bindParam(":saldo",$this->saldo,PDO::PARAM_STR);
        		$datos->bindParam(":porcentaje",$this->porcentaje,PDO::PARAM_STR);
         		$datos->bindParam(":tipo",$this->tipo,PDO::PARAM_STR);

        		$datos->execute();

        		header("Location:../formularios/usuario.php");
    		}
    		catch(Exception $e){
                echo"<html>
                           <head>
                               <link href='../estilomodal.css' rel='stylesheet'>
                               <link href='../Bootstrap/css/bootstrap.css' rel='stylesheet'>
                           </head>
                           <body>
                              <div id='ventanamodal'>
                                  <div class='alert alert-danger n' role='alert'>
                                 XXXX-ERROR TRANSACION NO REALIZADA  <a href='../formularios/usuario.php' class='alert-link'>  REGRESAR PAGINA PRINCIPAL</a>
                                  </div>
                               </div>
                           <body/>
                            </html>";
    		}
    		finally{
                 $this->conexionDB=null;
    		}
    	}
    	


    } 	

