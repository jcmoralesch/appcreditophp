<?php
    require"../conexion/conexion.php";

    class Pago extends Conexion{

    	private $tipo;
    	private $pagos;
        private $interes;
    	private $plazo;

    	public function Pago(){
    		parent::__construct();
    	}


    	public function setTipoPago($tipo){
    		$this->tipo=$tipo;
    	}

    	public function setPago($pagos){
    		$this->pagos=$pagos;
    	}

        public function setInteres($interes){
            $this->interes=$interes;
        }

    	public function setPlazo($plazo){
    		$this->plazo=$plazo;
    	} 

    	public function getTipoPago(){
    		return $this->tipo;
    	}

    	public function getPago(){
    		return $this->pagos;
    	}

    	public function getInteres(){
    		return $this->interes;
    	}

        public function getPlazo(){
            return $this->plazo;
        }


    	public function registrar(){
    		try{
                $sql="call registrarTipoPago(:tipo,:pagos,:interes,:plazo)";

                $datos=$this->conexionDB->prepare($sql);

        		$datos->bindParam(":tipo",$this->tipo,PDO::PARAM_STR);
        		$datos->bindParam(":pagos",$this->pagos,PDO::PARAM_STR);
                $datos->bindParam(":interes",$this->interes,PDO::PARAM_STR);
        		$datos->bindParam(":plazo",$this->plazo,PDO::PARAM_STR);

        		$datos->execute();

        		header("Location:../formularios/usuario.php");
    		}
    		catch(Exception $e){
                echo"<html>
                           <head>
                               <link href='../estilomodal.css' rel='stylesheet'>
                               <link href='../Bootstrap/css/bootstrap.css' rel='stylesheet'>
                           </head>
                           <body>
                              <div id='ventanamodal'>
                                  <div class='alert alert-danger n' role='alert'>
                                 XXXX-ERROR TRANSACION NO REALIZADA  <a href='../formularios/usuario.php' class='alert-link'>  REGRESAR PAGINA PRINCIPAL</a>
                                  </div>
                               </div>
                           <body/>
                            </html>";

    		}
    		finally{
                 $this->conexionDB=null;
    		}
    	}

        public function consultarTipoPago(){

            $sql="select nombre_tipo,no_pago,interes from tipo_pago";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

    } 	

