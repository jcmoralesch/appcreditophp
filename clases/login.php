<?php
    require_once"../conexion/conexion.php";

    class Login extends Conexion{

    	private $usuario;
    	private $password;
      private $id;
      private $direccion;
      private $sistema;



    	public function Login(){
    		parent::__construct();
    	}

    	public function setUsuario($usuario){
    		$this->usuario=$usuario;
    	}

    	public function setPassword($password){
    		$this->password=$password;
    	}

      public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function setSistema($sistema)
    {
        $this->sistema = $sistema;

        return $this;
    }


    	public function comprobar_login(){
    		try{

    			$contador=0;
                $privilegio;
	
	            $sql="select * from tabla_control where usuario=:login";

	            $resultado=$this->conexionDB->prepare($sql);	
		        $resultado->execute(array(":login"=>$this->usuario));
		        while($registro=$resultado->fetch(PDO::FETCH_ASSOC)){			
					
			       if(password_verify($this->password, $registro['password'])){
                      $privilegio=$registro['privilegio'];
                      $id=$registro['identificacion'];
				      $contador++;
			       }	
		        }
		        if($contador>0){
			       session_start();

                   if($privilegio=='NIVEL1'){
                       
                       $_SESSION["usuario"]=$this->usuario;
                       $_SESSION['sistema']=$this->sistema;
                       header("Location:../principal.php");
                   }
                   else if($privilegio=='NIVEL2'){
                       $_SESSION["usuario"]=$this->usuario;
                       $_SESSION['sistema']=$this->sistema;
                       $_SESSION["identificacion"]=$id;
                       header("Location:../formulariosl/principal.php");
                   }

                   else if($privilegio=='NIVEL3'){
                       $_SESSION["usuario"]=$this->usuario;
                       $_SESSION["identificacion"]=$id;
                       header("Location:../formulariosl/principalg.php");
                   }

                 
		        }
		        else{
			       header("Location:../index.html");
		        }		
    		}
    		catch(Exception $e){
    			die('Error no se pudo realizar la consulta' .$e->getMessage());
    		}
        finally{
           
        }
    	}


      public function registraracceso(){
            try{

                $datos=$this->conexionDB;
                $datos->beginTransaction();

                $sql="call registraracceso(:identificacion,:usuario,:direccion,:sistema)";

                $sgb=$datos->prepare($sql);

                $sgb->bindParam(":identificacion",$_SESSION["identificacion"],PDO::PARAM_STR);
                $sgb->bindParam(":usuario",$this->usuario,PDO::PARAM_STR);
                $sgb->bindParam(":direccion",$this->direccion,PDO::PARAM_STR);
                $sgb->bindParam(":sistema",$this->sistema,PDO::PARAM_STR);
                $sgb->execute();
               
                $datos->commit();
                return true;
            }
            catch(Exception $e){
                
                 $datos->rollBack();

                return false;
            }
            finally{
               $this->conexionDB=null;
            }
      }


      public function registrarcierresession(){
            try{

                $datos=$this->conexionDB;
                $datos->beginTransaction();

                $sql="call registrarcierresession(:usuario,:sistema)";

                $sgb=$datos->prepare($sql);

                $sgb->bindParam(":usuario",$this->usuario,PDO::PARAM_STR);
                $sgb->bindParam(":sistema",$this->sistema,PDO::PARAM_STR);
                $sgb->execute();
               
                $datos->commit();
                return true;
            }
            catch(Exception $e){
                
                 $datos->rollBack();

                return false;
            }
            finally{
                 $this->conexionDB=null;
            }
      }

      
    
}
?>
