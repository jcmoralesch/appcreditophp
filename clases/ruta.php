<?php
    require"../conexion/conexion.php";

    class Ruta extends Conexion{

    	private $id_ruta;
    	private $localizacion;
    	
    	public function Ruta(){
    		parent::__construct();
    	}


    	public function setIdRuta($id_ruta){
    		$this->id_ruta=$id_ruta;
    	}

    	public function setLocalizacion($localizacion){
    		$this->localizacion=$localizacion;
    	}

    	public function getIdRuta(){
    		return $this->id_ruta;
    	}

    	public function getLocalizacion(){
    		return $this->localizacion;
    	}


    	public function registrar(){
    		try{
                $sql="call registrarRuta(:id_ruta,:localizacion)";

                $datos=$this->conexionDB->prepare($sql);

        		$datos->bindParam(":id_ruta",$this->id_ruta,PDO::PARAM_STR);
        		$datos->bindParam(":localizacion",$this->localizacion,PDO::PARAM_STR);

        		$datos->execute();
                 
            header("Location:../formularios/usuario.php");

    		}
    		catch(Exception $e){
                
                 echo"<html>
                           <head>
                               <link href='../estilomodal.css' rel='stylesheet'>
                               <link href='../Bootstrap/css/bootstrap.css' rel='stylesheet'>
                           </head>
                           <body>
                              <div id='ventanamodal'>
                                  <div class='alert alert-danger n' role='alert'>
                                 XXXX-ERROR TRANSACION NO REALIZADA  <a href='../formularios/usuario.php' class='alert-link'>  REGRESAR PAGINA PRINCIPAL</a>
                                  </div>
                               </div>
                           <body/>
                            </html>";

                echo"<br>";
    		}
    		finally{
                 $this->conexionDB=null;
    		}
    	}



      public function finalizar(){
        try{
                $sql="call gestionarcalculos()";

                $datos=$this->conexionDB->prepare($sql);

            $datos->execute();

            return true;

        }
        catch(Exception $e){

          return false;
        }
        finally{
                 $this->conexionDB=null;
        }
      }


        
    }    


