<?php
    require"../conexion/conexion.php";

    class Usuario extends Conexion{

    	private $usuario;
    	private $password;
    	private $privilegio;
        private $id_empleado;
    	

    	public function Usuario(){
    		parent::__construct();
    	}


    	public function setUsuario($usuario){
    		$this->usuario=$usuario;
    	}

    	public function setPassword($password){
    		$this->password=$password;
    	}

    	public function setPrivilegio($privilegio){
    		$this->privilegio=$privilegio;
    	}

        public function setIdEmpleado($id_empleado){
            $this->id_empleado=$id_empleado;
        }

    	public function getUsuario(){
    		return $this->usuario;
    	}

    	public function getPassword(){
    		return $this->password;
    	}

    	public function getPrivilegio(){
    		return $this->privilegio;
    	}

    	public function getIdEmpleado(){
    		return $this->id_empleado;
    	}

    	public function registrar(){
    		try{
                $sql="call registrarUsuario(:usuario,:password,:privilegio,:id_empleado)";

                $datos=$this->conexionDB->prepare($sql);

        		$datos->bindParam(":usuario",$this->usuario,PDO::PARAM_STR);
        		$datos->bindParam(":password",$this->password,PDO::PARAM_STR);
        		$datos->bindParam(":privilegio",$this->privilegio,PDO::PARAM_STR);
                $datos->bindParam(":id_empleado",$this->id_empleado,PDO::PARAM_STR);

        		$datos->execute();

        		header("Location:../formularios/empleado.php");
    		}
    		catch(Exception $e){
                die('Error: '.$e->getMessage());
                echo"<html>
                           <head>
                               <link href='../estilomodal.css' rel='stylesheet'>
                               <link href='../Bootstrap/css/bootstrap.css' rel='stylesheet'>
                           </head>
                           <body>
                              <div id='ventanamodal'>
                                  <div class='alert alert-danger n' role='alert'>
                                 XXXX-ERROR TRANSACION NO REALIZADA  <a href='../formularios/empleado.php' class='alert-link'>  REGRESAR PAGINA PRINCIPAL</a>
                                  </div>
                               </div>
                           <body/>
                            </html>";

                echo"<br>";
    		}
    		finally{
                 $this->conexionDB=null;
    		}
    	}

        public function consultar(){

        }

    	public function actualizar($id){

    	}

    	


    } 	

