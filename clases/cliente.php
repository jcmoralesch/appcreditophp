<?php
    header('content-type: application/json; charset=utf-8');
    require_once"../conexion/conexion.php";


    class Cliente extends Conexion{

    	private $identificacion;
    	private $nombre;
    	private $apellido;
    	private $direccion;
        private $telefono;
        private $ruta;
        private $credito;
        private $pago;
        private $cantidad;
        private $r;



    	public function Cliente(){
    		parent::__construct();

            $this->r=array();

    	}

    	public function setIdentificacion($identificacion){
    		$this->identificacion=$identificacion;
    	}

    	public function setNombre($nombre){
    		$this->nombre=$nombre;
    	}

    	public function setApellido($apellido){
    		$this->apellido=$apellido;
    	}

    	public function setDireccion($direccion){
    		$this->direccion=$direccion;
    	}

        public function setTelefono($telefono){
            $this->telefono=$telefono;
        }

        public function setRuta($ruta){
            $this->ruta=$ruta;
        }

        public function setCredito($credito){
            $this->credito=$credito;
        }

        public function setPago($pago){
            $this->pago=$pago;
        }

        public function setCantidad($cantidad){
            $this->cantidad=$cantidad;
        }


    	public function getIdentficacion(){
    		return $this->identficacion;
    	}

    	public function getNombre(){
    		return $this->nombre;
    	}

    	public function getApellido(){
    		return $this->apellido;
    	}

    	public function getDireccion(){
    		return $this->direccion;
    	}

        public function getTelefono(){
            return $this->telefono;
        }

        public function getRuta(){
            return $this->ruta;
        }

        public function getCredito(){
            return $this->credito;
        }

        public function getPago(){
            return $this->pago;
        }

        public function getCantidad(){
            return $this->cantidad;
        }



    	public function registrar(){
            try{

                $datos=$this->conexionDB;
                $datos->beginTransaction();

                $sql="call registrarCliente(:identificacion,:nombre,:apellido,:direccion,:telefono,:ruta,:credito,:pago,:cantidad)";

                $sgb=$datos->prepare($sql);

                $sgb->bindParam(":identificacion",$this->identificacion,PDO::PARAM_STR);
                $sgb->bindParam(":nombre",$this->nombre,PDO::PARAM_STR);
                $sgb->bindParam(":apellido",$this->apellido,PDO::PARAM_STR);
                $sgb->bindParam(":direccion",$this->direccion,PDO::PARAM_STR);
                $sgb->bindParam(":telefono",$this->telefono,PDO::PARAM_STR);
                $sgb->bindParam(":ruta",$this->ruta,PDO::PARAM_STR);
                $sgb->bindParam(":credito",$this->credito,PDO::PARAM_STR);
                $sgb->bindParam(":pago",$this->pago,PDO::PARAM_STR);
                $sgb->bindParam(":cantidad",$this->cantidad,PDO::PARAM_STR);

                $sgb->execute();

                $datos->commit();
                return true;
            }
            catch(Exception $e){

                 $datos->rollBack();

                return false;
            }
            finally{
                 $this->conexionDB=null;
            }
    	}

        public function mostrarClientes(){

            $sql="select identificacion,nombre,apellido,telefono,id_ruta from cliente limit 15";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


         public function mostrarClientesActivos(){

            $sql="select * from datos_cliente ";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


        public function mostrarClientesPorNombre($ruta,$nombre){

            $sql="select * from datos_cliente where id_ruta='".$ruta."' and cliente like '%".$nombre."%' limit 10";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


        public function mostrarClientesPorRuta($cad,$cad1){

            $pr=$cad."%".$cad1;
            $sql="select * from datos_cliente where id_ruta like '$pr' limit 15";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


        public function consultarPorRuta($ruta,$credito){

            $sql="select  distinct C.identificacion,concat(C.nombre,' ',C.apellido) as cliente,C.telefono,R.monto,E.saldo,
concat(E.pagos_realizados+E.pagos)as cuotas,R.cuota,R.fecha_vencimiento,R.id_credito,
E.pagos_realizados,M.mora_generada,M.dias_mora,M.total_de_mora,M.adelantado
from cliente C,credito R,estado_cuenta E, mora M, fecha_pagos D
where C.identificacion=R.identificacion and R.id_credito=M.id_credito
and R.id_credito=E.id_credito and R.id_credito=D.id_credito and C.id_ruta='".$this->ruta."'
and R.id_tipo_credito='".$this->credito."' and (D.fecha=current_date  or R.fecha_vencimiento<current_date or M.dias_mora>0)and R.status='P' and E.status='P'  and  M.adelantado=0
order by R.fecha_registro asc";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function consultarPorRutaFecha($ruta,$credito,$fecha){

            $sql="select  distinct C.identificacion,concat(C.nombre,' ',C.apellido) as cliente,C.telefono,R.monto,E.saldo,
concat(E.pagos_realizados+E.pagos)as cuotas,R.cuota,R.fecha_vencimiento,
E.pagos_realizados,M.mora_generada,M.dias_mora,M.total_de_mora,M.adelantado
from cliente C,credito R,estado_cuenta E, mora M, fecha_pagos D
where C.identificacion=R.identificacion and R.id_credito=M.id_credito
and R.id_credito=E.id_credito and R.id_credito=D.id_credito and C.id_ruta='".$this->ruta."'
and R.id_tipo_credito='".$this->credito."' and (D.fecha='".$fecha."'  or R.fecha_vencimiento<'".$fecha."' or M.dias_mora>0)and R.status='P' and E.status='P' and M.adelantado=0
order by R.fecha_registro asc";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }



        public function consultarEmpleado($ruta){

            $sql="select nombre,apellido from empleado where id_ruta='".$this->ruta."'";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function consultarRutaEmpleado($id){

            $sql="select id_ruta, nombre,apellido from empleado where identificacion='".$id."';";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


        public function consultarPerfilCliente($id){

            $sql="select * from perfil_cliente where id_credito='".$id."'";

            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }

        public function movimientoCredito($id){

            $sql="select D.cantidad,round((D.cantidad/C.cuota)) as cuota,D.mora,date_format(D.fecha_deposito,'%d/%m/%Y') from  deposito D,credito C
where D.id_credito=C.id_credito and D.id_credito='".$id."'";

            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->execute(array());

            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $sentencia->closeCursor();

            return $resultado;

            $this->conexionDB=null;
        }


         public function consultar_usuario(){
            $sql="select * from cliente where identificacion=?";
            $sentencia=$this->conexionDB->prepare($sql);

            $sentencia->bindParam(1,$this->identificacion,PDO::PARAM_STR);
            $sentencia->execute();
            $contar=$sentencia->rowCount();

            if($contar===0){
                return true;

            }
            else{
                return false;
            }

            $sentencia->closeCursor();
            $this->conexionDB=null;

        }

        public function consultarFechaPago($id){

            $sql="select obtener_fecha_pago('".$id."')";
            $sentencia=$this->conexionDB->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchColumn();
            $sentencia->closeCursor();
            return $resultado;

            $this->conexionDB=null;
        }

        public function consultarCodigosAsignados($ruta){

          $sql="select identificacion from cliente where identificacion like '".$ruta."%'";

          $sentencia=$this->conexionDB->prepare($sql);

          $sentencia->execute(array());

          $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

          $sentencia->closeCursor();

          return $resultado;

          $this->conexionDB=null;

        }


        public function ubicacion(){

            $dom = new DOMDocument("1.0");
            $node = $dom->createElement("markers");
            $parnode = $dom->appendChild($node);


            $sql = "SELECT * FROM cliente where cx is not null and cy is not null";

            $result=$this->conexionDB->prepare($sql);

            $result->execute();

            header("Content-type: text/xml");

             while ($row = @$result->fetch()){

              $node = $dom->createElement("marker");
              $newnode = $parnode->appendChild($node);
              $newnode->setAttribute("name",$row['nombre']);
              $newnode->setAttribute("apellido",$row['apellido']);
              $newnode->setAttribute("address", $row['direccion']);
              $newnode->setAttribute("lat", $row['cx']);
              $newnode->setAttribute("lng", $row['cy']);
              $newnode->setAttribute("type", $row['id_ruta']);
            }

            echo $dom->saveXML();

            $result->closeCursor();

            $this->conexionDB=null;
        }






        public function ubicacionruta(){

            $dom = new DOMDocument("1.0");
            $node = $dom->createElement("markers");
            $parnode = $dom->appendChild($node);


            $sql = "SELECT * FROM cliente where cx is not null and cy is not null and id_ruta='".$this->ruta."'";

            $result=$this->conexionDB->prepare($sql);

            $result->execute();

            header("Content-type: text/xml");

             while ($row = @$result->fetch()){

              $node = $dom->createElement("marker");
              $newnode = $parnode->appendChild($node);
              $newnode->setAttribute("name",$row['nombre']);
              $newnode->setAttribute("apellido",$row['apellido']);
              $newnode->setAttribute("address", $row['direccion']);
              $newnode->setAttribute("lat", $row['cx']);
              $newnode->setAttribute("lng", $row['cy']);
              $newnode->setAttribute("type", $row['id_ruta']);
            }

            echo $dom->saveXML();

            $result->closeCursor();

            $this->conexionDB=null;
        }

    }
