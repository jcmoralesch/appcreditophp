<?php
 session_start();
 if(!isset($_SESSION["usuario"])){
      header("Location:../index.html");
  }

include 'plantilla.php';
include '../clases/cliente.php';

$cliente= new Cliente();

$array_cliente=$cliente->mostrarClientesActivos();

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');

$pdf->SetFillColor(232,232,232);

$pdf->SetFont('Arial','B',7);
$pdf->Cell(8,4,'No.',1,0,'C',1);
$pdf->Cell(15,4,utf8_decode('Código'),1,0,'C',1);
$pdf->Cell(50,4,'Cliente',1,0,'C',1);
$pdf->Cell(13,4,utf8_decode('Teléfono'),1,0,'C',1);
$pdf->Cell(13,4,'Capital',1,0,'C',1);
$pdf->Cell(13,4,'Saldo',1,0,'C',1);
$pdf->Cell(9,4,'Cuotas',1,0,'C',1);
$pdf->Cell(6,4,'CP',1,0,'C',1);
$pdf->Cell(10,4,'Cuota',1,0,'C',1);
$pdf->Cell(11,4,'Total',1,1,'C',1);

$var=1;
$total=0;
$contador=1;

foreach ($array_cliente as $elemento) {
	$pdf->Cell(8,4,$contador++,1,0,'C');
	$pdf->Cell(15,4,$elemento['id_ruta'].$var++,1,0,'C');
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(50,4,utf8_decode($elemento['cliente']),1,0,'C');
	$pdf->SetFont('Arial','',7);
	$pdf->Cell(13,4,$elemento['telefono'],1,0,'C');
	$pdf->Cell(13,4,$elemento['monto'],1,0,'C');
	$pdf->Cell(13,4,$elemento['saldo'],1,0,'C');
	$pdf->Cell(9,4,$elemento['cuotas'],1,0,'C');
	$pdf->Cell(6,4,$elemento['pagos_realizados'],1,0,'C');
	$pdf->Cell(10,4,$elemento['cuota'],1,0,'C');
	$pdf->Cell(11,4,$elemento['cuota'],1,1,'C');

	$total=$total+$elemento['cuota'];
}
$pdf->SetFont('Arial','B',8);
$pdf->Cell(40,5,'Total de clientes: '.($var-1),1,0,'C');
$pdf->Cell(40,5,'Total a cobrar: '.$total,1,0,'C');


//$pdf->Output('D','Reporte_clientes.pdf');
$pdf->Output();

?>