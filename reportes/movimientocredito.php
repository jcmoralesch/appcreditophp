<?php
 session_start();
 if(!isset($_SESSION["usuario"])){
      header("Location:../index.html");
  }

include 'plantilla.php';
include '../clases/cliente.php';

$id=$_POST['cod'];


$pago= new Cliente();

$array_cantidad=$pago->movimientoCredito($id);


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');

$pdf->SetFillColor(232,232,232);

$pdf->SetFont('Arial','B',9);
$pdf->Cell(24,6,utf8_decode('Cantidad'),1,0,'C',1);
$pdf->Cell(32,6,'No. pagos aplicadas',1,0,'C',1);
$pdf->Cell(32,6,'Mora pagada',1,0,'C',1);
$pdf->Cell(32,6,utf8_decode('Fecha'),1,1,'C',1);

$total=0;
foreach ($array_cantidad as $elemento) {
	$pdf->Cell(24,6,$elemento['cantidad'],'B'.'L',0,'C');
	$pdf->Cell(32,6,$elemento['cuota'],'B',0,'C');
	$pdf->Cell(32,6,$elemento['mora'],'B',0,'C');
	$pdf->Cell(32,6,$elemento["date_format(D.fecha_deposito,'%d/%m/%Y')"],'B'.'R',1,'C');
    $total=$total+$elemento['cantidad'];
}
$pdf->Cell(50,6,'TOTAL PAGADA: '.$total,0,0,'C',1);
//$pdf->Output('D','Reporte_clientes.pdf');
$pdf->Output();

?>