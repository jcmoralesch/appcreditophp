<?php session_start(); if(!isset($_SESSION["usuario"])){header("Location:../index.html");}

include 'plantilla.php';
include '../clases/tipopago.php';

$cantidad=$_POST['cantidad'];

$pago= new Pago();

$array_cantidad=$pago->consultarTipoPago();


$datos='';


$datos.='
<div class="p-1 mb-1 bg-success text-white text-lg-center">CANTIDAD  '.$cantidad.'</div>
<table class="table table-responsive">
  <thead class="text-center">
    <tr>
      <th scope="col">Crédito</th>
      <th scope="col">No. de pagos</th>
      <th scope="col">Interés</th>
      <th scope="col">Total interés</th>
      <th scope="col">Capital más interés</th>
      <th scope="col">Cuota</th>
    </tr>
  </thead>
  <tbody class="text-center">';



foreach ($array_cantidad as $elemento) {	
    
      $val=($elemento['interes']*$cantidad)+$cantidad;
      $cuotas=(($elemento['interes']*$cantidad)+$cantidad)/$elemento['no_pago'];
      
     $datos.= '<tr>
      <th scope="row">'.$elemento['nombre_tipo'].'</th>
      <td>'.$elemento['no_pago'].'</td>
      <td>'.$elemento['interes'].'</td>
      <td>'.$elemento['interes']*$cantidad.'</td>
     
      <td>'.$val.'</td>
      <td>'.$cuotas.'</td>  
    </tr>';

}

$datos.=' </tbody>
</table>';

echo $datos;
?>