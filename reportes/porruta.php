<?php
 session_start();
 if(!isset($_SESSION["usuario"])){
      header("Location:../index.html");
  }

include 'plantilla.php';
include '../clases/cliente.php';

$ruta=$_POST['ruta'];
$tipoCred=$_POST['tipocredito'];

$cliente= new Cliente();

$array_cliente=$cliente->consultarPorRuta($cliente->setRuta($ruta),$cliente->setCredito($tipoCred));
$empleado=$cliente->consultarEmpleado($cliente->setRuta($ruta));

$pos=substr($ruta, 1,1);

if($ruta=="R".$pos."DCH"){

	$ruta="Ruta $pos $tipoCred de chichicastenango";
}
else if($ruta=="R".$pos."DU"){
	$ruta="Ruta $pos $tipoCred de Uspantan";
}
else if($ruta=="R".$pos."SU"){
	$ruta="Ruta $pos $tipoCred de Uspantan";
}
else if($ruta=="R".$pos."DC"){
	$ruta="Ruta $pos $tipoCred de Cunen";
}


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');

$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(50,5,$ruta,0,0,'L');
foreach($empleado as $id){
    $pdf->Cell(30,5,'Es responsable: '.utf8_decode($id['nombre']).' '.utf8_decode($id['apellido']),0,1,'L');
}

$pdf->SetFont('Arial','B',7);
$pdf->Cell(8,4,'No.',1,0,'C',1);
$pdf->Cell(15,4,utf8_decode('Código'),1,0,'C',1);
$pdf->Cell(60,4,'Cliente',1,0,'C',1);
$pdf->Cell(13,4,utf8_decode('Teléfono'),1,0,'C',1);
$pdf->Cell(13,4,'Capital',1,0,'C',1);
$pdf->Cell(13,4,'Saldo',1,0,'C',1);
$pdf->Cell(9,4,'Cuotas',1,0,'C',1);
$pdf->Cell(6,4,'CP',1,0,'C',1);
$pdf->Cell(12,4,'Vencidas',1,0,'C',1);
$pdf->Cell(10,4,'Cuota',1,0,'C',1);
$pdf->Cell(11,4,'Total',1,0,'C',1);
$pdf->Cell(10,4,'Mora',1,0,'C',1);
$pdf->Cell(12,4,'TOTAL C',1,1,'C',1);

$var=1;
$contador=1;
$total=0;
$totalMora=0;
$totalVencida=0;
$totalCobro=0;
$cal=0;
$conti="";

$fechah = date('Y-m-d H:i:s');
$nuevafecha = strtotime ('-1 day' , strtotime ( $fechah ) ) ;
$nuevafecha = date ('Y-m-d H:i:s' , $nuevafecha );

if($tipoCred==="SEMANAL"){

  foreach ($array_cliente as $elemento) {
    $conti=$elemento['fecha_vencimiento'];

     if($conti < $nuevafecha){
       $pdf->setTextColor(255, 0, 0);

       $pdf->Cell(8,4,$contador++,1,0,'C');
       $pdf->Cell(15,4,$elemento['identificacion'],1,0,'C');
       $pdf->SetFont('Arial','',6);
       $pdf->Cell(60,4,utf8_decode($elemento['cliente']),1,0,'');
       $pdf->SetFont('Arial','',7);
       $pdf->Cell(13,4,$elemento['telefono'],1,0,'C');
       $pdf->Cell(13,4,$elemento['monto'],1,0,'C');
       $pdf->Cell(13,4,$elemento['saldo'],1,0,'C');
       $pdf->Cell(9,4,$elemento['cuotas'],1,0,'C');
       $pdf->Cell(6,4,$elemento['pagos_realizados'],1,0,'C');
       $cal=$elemento['dias_mora']+1;
       $pdf->Cell(12,4,$cal,1,0,'C');
       $pdf->Cell(10,4,$elemento['cuota'],1,0,'C');
       $pdf->Cell(11,4,$elemento['cuota']*$cal,1,0,'C');
       $pdf->Cell(10,4,$elemento['mora_generada'],1,0,'C');
       $pdf->Cell(12,4,$elemento['mora_generada'] +($elemento['saldo']),1,1,'C');
       $var++;
     }
     else if($conti >= $nuevafecha ){

       $pdf->setTextColor(0,0,0);
       if($cliente->consultarFechaPago($elemento['id_credito'])==0){
        $pdf->setTextColor(16,23,250);
         $cal=$elemento['dias_mora'];
       }else{
            $cal=$elemento['dias_mora']+1;
       }
       $pdf->Cell(8,4,$contador++,1,0,'C');
       $pdf->Cell(15,4,$elemento['identificacion'],1,0,'C');
       $pdf->SetFont('Arial','',6);
       $pdf->Cell(60,4,utf8_decode($elemento['cliente']),1,0,'');
       $pdf->SetFont('Arial','',7);
       $pdf->Cell(13,4,$elemento['telefono'],1,0,'C');
       $pdf->Cell(13,4,$elemento['monto'],1,0,'C');
       $pdf->Cell(13,4,$elemento['saldo'],1,0,'C');
       $pdf->Cell(9,4,$elemento['cuotas'],1,0,'C');
       $pdf->Cell(6,4,$elemento['pagos_realizados'],1,0,'C');
       $pdf->Cell(12,4,$cal,1,0,'C');
       $pdf->Cell(10,4,$elemento['cuota'],1,0,'C');
       $pdf->Cell(11,4,$elemento['cuota']*$cal,1,0,'C');
       $pdf->Cell(10,4,$elemento['mora_generada'],1,0,'C');
       $pdf->Cell(12,4,$elemento['mora_generada'] +($elemento['cuota']*$cal),1,1,'C');
       $var++;

     }

   $total=$total+$elemento['cuota'];
   $totalVencida=$totalVencida+$elemento['cuota']*$cal;
   $totalMora=$totalMora+$elemento['mora_generada'];
   $totalCobro=$totalCobro+$elemento['mora_generada'] +($elemento['cuota']*$cal);
  }
}
else{
  foreach ($array_cliente as $elemento) {
  	 $conti=$elemento['fecha_vencimiento'];
  		if($conti < $nuevafecha){
  			$pdf->setTextColor(255, 0, 0);

  			$pdf->Cell(8,4,$contador++,1,0,'C');
  			$pdf->Cell(15,4,$elemento['identificacion'],1,0,'C');
  			$pdf->SetFont('Arial','',6);
  			$pdf->Cell(60,4,utf8_decode($elemento['cliente']),1,0,'');
  			$pdf->SetFont('Arial','',7);
  			$pdf->Cell(13,4,$elemento['telefono'],1,0,'C');
  			$pdf->Cell(13,4,$elemento['monto'],1,0,'C');
  			$pdf->Cell(13,4,$elemento['saldo'],1,0,'C');
  			$pdf->Cell(9,4,$elemento['cuotas'],1,0,'C');
  			$pdf->Cell(6,4,$elemento['pagos_realizados'],1,0,'C');
  			$cal=$elemento['dias_mora']+1;
  			$pdf->Cell(12,4,$cal,1,0,'C');
  			$pdf->Cell(10,4,$elemento['cuota'],1,0,'C');
  			$pdf->Cell(11,4,$elemento['cuota']*$cal,1,0,'C');
  			$pdf->Cell(10,4,$elemento['mora_generada'],1,0,'C');
  			$pdf->Cell(12,4,$elemento['mora_generada'] +($elemento['saldo']),1,1,'C');
  			$var++;
  		}


  		else{
  			$pdf->setTextColor(0,0,0);

  	    $pdf->Cell(8,4,$contador++,1,0,'C');
  			$pdf->Cell(15,4,$elemento['identificacion'],1,0,'C');
  			$pdf->SetFont('Arial','',6);
  			$pdf->Cell(60,4,utf8_decode($elemento['cliente']),1,0,'');
  			$pdf->SetFont('Arial','',7);
  			$pdf->Cell(13,4,$elemento['telefono'],1,0,'C');
  			$pdf->Cell(13,4,$elemento['monto'],1,0,'C');
  			$pdf->Cell(13,4,$elemento['saldo'],1,0,'C');
  			$pdf->Cell(9,4,$elemento['cuotas'],1,0,'C');
  			$pdf->Cell(6,4,$elemento['pagos_realizados'],1,0,'C');
  			$cal=$elemento['dias_mora']+1;
  			$pdf->Cell(12,4,$cal,1,0,'C');
  			$pdf->Cell(10,4,$elemento['cuota'],1,0,'C');
  			$pdf->Cell(11,4,$elemento['cuota']*$cal,1,0,'C');
  			$pdf->Cell(10,4,$elemento['mora_generada'],1,0,'C');
  			$pdf->Cell(12,4,$elemento['mora_generada'] +($elemento['cuota']*$cal),1,1,'C');
  			$var++;

  		}

  	$total=$total+$elemento['cuota'];
  	$totalVencida=$totalVencida+$elemento['cuota']*$cal;
  	$totalMora=$totalMora+$elemento['mora_generada'];
  	$totalCobro=$totalCobro+$elemento['mora_generada'] +($elemento['cuota']*$cal);
  }
}

$pdf->SetFont('Arial','B',8);
$pdf->Cell(35,5,'Total de clientes: '.($var-1),1,0,'C');
$pdf->Cell(24,5,'A COBRAR EN: ',1,0,'C');
$pdf->Cell(23,5,'Cuotas: '.$total,1,0,'C');
$pdf->Cell(30,5,'Vencidas: '.$totalVencida,1,0,'C');
$pdf->Cell(30,5,'Mora: '.$totalMora,1,0,'C');
$pdf->Cell(40,5,'Vencidas + mora: '.$totalCobro,1,1,'C');
$pdf->Output();

?>
