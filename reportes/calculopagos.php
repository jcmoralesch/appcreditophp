<?php
 session_start();
 if(!isset($_SESSION["usuario"])){
      header("Location:../index.html");
  }

include 'plantilla.php';
include '../clases/tipopago.php';

$cantidad=$_POST['cantidad'];

$pago= new Pago();

$array_cantidad=$pago->consultarTipoPago();


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');

$pdf->SetFillColor(232,232,232);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(40,8,'Cantidad: '.$cantidad,0,1,'C',1);

$pdf->SetFont('Arial','B',9);
$pdf->Cell(32,6,utf8_decode('Crédito'),1,0,'C',1);
$pdf->Cell(26,6,'No. de pagos',1,0,'C',1);
$pdf->Cell(16,6,utf8_decode('Interés'),1,0,'C',1);
$pdf->Cell(22,6,utf8_decode('Total interés'),1,0,'C',1);
$pdf->Cell(30,6,utf8_decode('Capital más interés'),1,0,'C',1);
$pdf->Cell(16,6,'Cuota',1,1,'C',1);


foreach ($array_cantidad as $elemento) {
	$pdf->Cell(32,6,$elemento['nombre_tipo'],1,0,'C');
	$pdf->Cell(26,6,$elemento['no_pago'],1,0,'C');
	$pdf->Cell(16,6,$elemento['interes'],1,0,'C');
	$pdf->Cell(22,6,$elemento['interes']*$cantidad,1,0,'C');
	$pdf->Cell(30,6,($elemento['interes']*$cantidad)+$cantidad,1,0,'C');
	
    $cuotas=(($elemento['interes']*$cantidad)+$cantidad)/$elemento['no_pago'];

    $pdf->Cell(16,6,$cuotas,1,1,'C');

}

//$pdf->Output('D','Reporte_clientes.pdf');
$pdf->Output();

?>