<?php
 session_start();
 if(!isset($_SESSION["usuario"])){
      header("Location:../index.html");
  }

include 'plantilla.php';
include '../clases/tipocredito.php';

$id=$_POST['identificacion'];

$tipoC= new TipoCredito();
 
$array_fechas=$tipoC-> generarCalendario($id);


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');

$pdf->SetFillColor(232,232,232);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(40,8,'Identificacion: '.$id,0,1,'C',1);


$pdf->SetFont('Arial','B',7);
$pdf->Cell(30,6,'No. de pagos',1,0,'C',1);
$pdf->Cell(30,6,'Fechas de pago',1,1,'C',1);
 
 $var=1;
 $nombre;
 $apellido;
 $ruta;

foreach ($array_fechas as $elemento) {
    $pdf->Cell(30,5,$var++,1,0,'C');
    $pdf->Cell(30,5,$elemento['fecha'],1,1,'C');
         $nombre =$elemento['nombre'];
         $apellido =$elemento['apellido'];
         $ruta=$elemento['id_ruta'];

}
$pdf->SetFont('Arial','B',9);
$pdf->Cell(60,6,'Cliente:'.$nombre.' '.$apellido.' Ruta '.$ruta,0,1,'C');

//$pdf->Output('D','Reporte_clientes.pdf');
$pdf->Output();

?>