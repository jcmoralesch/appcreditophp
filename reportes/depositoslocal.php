<?php session_start(); if(!isset($_SESSION["usuario"])){header("Location:../index.html");}

include 'plantilla.php';
include '../clases/tipocredito.php';

$fecha1=$_POST['fecha1'];
$fecha2=$_POST['fecha2'];

$pago= new TipoCredito();

$array_cantidad=$pago->depositos($fecha1,$fecha2);

$datos='';


$datos.='
<div class="p-1 mb-1 bg-info text-white text-lg-center">COBROS DEL  '.$fecha1.' AL '.$fecha2.'   </div>
<table class="table table-sm table-striped table-hover">
  <thead>
    <tr>
      <th scope="col">Ruta</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Mora</th>
      <th scope="col">Fecha</th>
    </tr>
  </thead>
  <tbody>';


$total=0;
$total_mora=0;
foreach ($array_cantidad as $elemento) {
	$datos.= '<tr>
      <th scope="row">'.$elemento['id_ruta'].'</th>
      <td>'.$elemento['sum(D.cantidad)'].'</td>
      <td>'.$elemento['sum(D.mora)'].'</td>
      <td>'.$elemento["date_format(D.fecha_deposito,'%d/%m/%Y')"].'</td>
    </tr>';
    $total=$total+$elemento['sum(D.cantidad)'];
    $total_mora=$total_mora+$elemento['sum(D.mora)'];
}

 $datos.=' </tbody>
</table>';

echo $datos;
echo '<p class="h6">TOTAL COBRADO: ' .$total.'&nbsp; &nbsp; &nbsp; &nbsp;  TOTAL MORA COBRADO '. $total_mora.' </p>';


?>




  	
  
