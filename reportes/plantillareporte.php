<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
  <meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">
<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<title>Reportes</title>
</head>

<body id="pag">

     <?php
          require_once "../clases/tipocredito.php";
          $tCredito = new TipoCredito();
      ?>

           <header>
              <img src="../formularios/logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>


          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Área de reportes</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../formularios/pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="../formularios/cliente.php">Clientes</a>
                    <a class="dropdown-item" href="../formularios/empleado.php">Empleados</a>
                    <a class="dropdown-item" href="../formularios/usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../objetos/clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="../formularios/clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="../formularios/usuarioindividual.php">Usuarios</a>
                    <a class="dropdown-item" href="../formularios/recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="../objetos/clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="../formularios/consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../formularios/mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="../objetos/revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="../objetos/actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="../formularios/ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="../formularios/cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../principal.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Reporte clientes</li>
          </ol>
        </nav>

        <div id="espacio"></div>
        <div class="container-fluid">
          <div class="row">
               <div class="col-lg-5 col-sm-12">
                <div class="p-1 mb-1 bg-secondary text-white text-center">Generar reportes de rutas en PDF</div>
           <table class="table table-sm">
               <thead class="text-center">
                    <tr>
                      <th scope="col">Ruta</th>
                      <th scope="col">Credito</th>
                      <th scope="col">Fecha</th>
                      <th scope="col">Generar</th>
                    </tr>
                  </thead>
              <tbody>
                <form action="porruta.php" method="post" target="_blank">
                <tr>
                  <td><select name="ruta" class="form-control form-control-sm">
                                                 <?php
                                                      $array_ruta=$tCredito->consultarRuta();

                                                    foreach($array_ruta as $elemento){
                                                       echo"<option>";
                                                       echo $elemento['id_ruta'];
                                                       echo"</option>";
                                                      }
                                                   ?>
                                            </select></td>
                  <td><select name="tipocredito" class="form-control form-control-sm">
                                                 <?php
                                                      $array_ruta=$tCredito->consultarTCredito();

                                                    foreach($array_ruta as $elemento){
                                                       echo"<option>";
                                                       echo $elemento['id_credito'];
                                                       echo"</option>";
                                                      }
                                                   ?>
                                            </select></td>
                  <td></td>
                  <td><input type="submit" name="registrar" value="PDF" class="btn btn-outline-info btn-sm btn-block"></td>

                </tr>
                </form>

                <form action="porrutafecha.php" method="post" target="_blank">
                <tr>
                  <td><select name="ruta" class="form-control form-control-sm">
                                                 <?php
                                                      $array_ruta=$tCredito->consultarRuta();

                                                    foreach($array_ruta as $elemento){
                                                       echo"<option>";
                                                       echo $elemento['id_ruta'];
                                                       echo"</option>";
                                                      }
                                                   ?>
                                            </select></td>
                  <td><select name="tipocredito" class="form-control form-control-sm">
                                                 <?php
                                                      $array_ruta=$tCredito->consultarTCredito();

                                                    foreach($array_ruta as $elemento){
                                                       echo"<option>";
                                                       echo $elemento['id_credito'];
                                                       echo"</option>";
                                                      }
                                                   ?>
                                            </select></td>
                  <td><input type="text" name="fecha" require  class="form-control form-control-sm date" required></td>
                  <td><input type="submit" name="registrar" value="PDF" class="btn btn-outline-info btn-sm btn-block"></td>
                </tr>
                </form>
              </tbody>
            </table>

            <div id="espacio"></div>
            <div class="p-1 mb-1 bg-secondary text-white text-center">Reportes en PDF y en linea</div>
            <table class="table table-sm table-responsive">
                <thead class="text-center">
                  <tr>
                    <th scope="col">De fecha</th>
                    <th scope="col">A fecha</th>
                    <th scope="col">Ruta</th>
                    <th scope="col">PDF</th>
                    <th scope="col">VER</th>
                  </tr>
                </thead>
                <tbody>
                  <form id="consultarcobro" action="depositos.php" method="post" target="_blank">
                  <tr>
                    <th scope="row"><input type="text" name="fecha1" require class="form-control form-control-sm date" required></th>
                    <td><input type="text" name="fecha2" require  class="form-control form-control-sm date"></td>
                    <td></td>
                    <td><input type="submit" name="registrar" value="COBROS" class="btn btn-outline-info btn-sm  btn-block"></td>
                    <td><button type="button" id="btncobro" name="registrar" class="btn btn-outline-info btn-sm btn-block">COBROS</button></td>
                  </tr>
                </form>

                <form id="consultarcolocacion" action="colocacion.php" method="post" target="_blank">
                  <tr>
                    <th scope="row"><input type="text" name="fecha1" require  class="form-control form-control-sm date" required></th>
                    <td><input type="text" name="fecha2" require  class="form-control form-control-sm date"></td>
                    <td></td>
                    <td><input type="submit" name="registrar" value="COLOCACIÓN" class="btn btn-outline-info btn-sm  btn-block"></td>
                    <td><button type="button" id="btncolocacion" name="registrar"  class="btn btn-outline-info btn-sm btn-block">COLOCACIÓN</button></td>
                  </tr>
                </form>
                  <form  id="consultarrecaudo" action="calcularrecaudo.php" method="post" target="_blank">
                  <tr>
                    <th scope="row"><input type="text" name="fecha1" require  class="form-control form-control-sm date" required></th>
                    <td><input type="text" name="fecha2" require  class="form-control form-control-sm date"></td>
                    <td><select name="ruta" class="form-control form-control-sm">
                                                 <?php
                                                      $array_ruta=$tCredito->consultarRuta();

                                                    foreach($array_ruta as $elemento){
                                                       echo"<option>";
                                                       echo $elemento['id_ruta'];
                                                       echo"</option>";
                                                      }
                                                   ?>
                                            </select></td>
                    <td><input type="submit" name="registrar" value="Recaudo" class="btn btn-outline-info btn-sm btn-block"></td>
                    <td><button type="button" name="registrar" id="btnrecaudo" class="btn btn-outline-info btn-sm btn-block">Recaudo</button></td>
                  </tr>
                </form>
                </tbody>
              </table>


              <div id="espacio"></div>
            <div class="p-1 mb-1 bg-secondary text-white text-center">Calcular Cuotas</div>

              <table class="table table-sm">
                  <thead class="text-center">
                    <tr>
                      <th scope="col">Cantidad</th>
                      <th scope="col">Calcular</th>
                    </tr>
                  </thead>
                  <tbody>
                    <form  id="consultarcalculo" method="post">
                    <tr>
                      <th scope="row"><input type="text" name="cantidad" require placeholder="ingrese cantidad Ej.(1000.00)" class="form-control form-control-sm"></th>
                      <td><button type="button" name="registrar" id="btncalculo" class="btn btn-outline-info btn-sm btn-block">Generar calculo</button></td>
                    </tr>
                  </form>
                  </tbody>
              </table>



              <div id="espacio"></div>

      <div class="p-1 mb-1 bg-secondary text-white text-center">Consultar códigos libres</div>
      <table class="table table-sm ">
          <thead class="text-center">
            <tr>
              <th scope="col">Ruta</th>
              <th scope="col">VER</th>
            </tr>
          </thead>
          <tbody>

            <form  id="consultarasignado" action="" method="post">
            <tr>

              <th scope="row"><select name="ruta" class="form-control form-control-sm">
                                          <?php
                                                $array_ruta=$tCredito->consultarRuta();

                                              foreach($array_ruta as $elemento){
                                                 echo"<option>";
                                                 echo $elemento['id_ruta'];
                                                 echo"</option>";
                                                }
                                             ?>
                                      </select>
                                    </th>
              <td><button type="button" name="registrar" id="btnasignado" class="btn btn-outline-info btn-sm btn-block">Códigos</button></td>
            </tr>
          </form>
          </tbody>
        </table>

               </div>

               <div class="col-lg-7">
                    <div id="resultadoconsulta"></div>

               </div>
          </div>
        </div>

          <footer>

           </footer>

     <script src="../view/js/jquery-3.2.1.min.js"></script>
     <script src="../view/js/popper.min.js"></script>
     <script src="../view/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


     <script type="text/javascript">
         $(document).ready(function(){

             $(".date").datepicker({dateFormat: 'yy/mm/dd'});

              $("#btncobro").click(function(){

                   var dataString = $('#consultarcobro').serialize();

                    $.ajax({
                      url:"depositoslocal.php",
                      data : dataString,
                      type : 'POST',
                      dataType :'html',
                      success:function(resultado) {
                              $('#resultadoconsulta').html(resultado);
                      },error : function(xhr, status) {
                          if (xhr.status == 404) {

                             alert('Requested page not found [404]');

                            }
                      }
                         });
              });



               $("#btncolocacion").click(function(){

                   var dataString = $('#consultarcolocacion').serialize();

                    $.ajax({
                      url:"colocacionlocal.php",
                      data : dataString,
                      type : 'POST',
                      dataType :'html',
                      success:function(resultado) {
                              $('#resultadoconsulta').html(resultado);
                      },error : function(xhr, status) {
                          if (xhr.status == 404) {

                             alert('Requested page not found [404]');

                            }
                      }
                         });
              });

               $("#btnrecaudo").click(function(){

                   var dataString = $('#consultarrecaudo').serialize();

                    $.ajax({
                      url:"calcularrecaudolocal.php",
                      data : dataString,
                      type : 'POST',
                      dataType :'html',
                      success:function(resultado) {
                              $('#resultadoconsulta').html(resultado);
                      },error : function(xhr, status) {
                          if (xhr.status == 404) {

                             alert('Requested page not found [404]');

                            }
                      }
                         });
              });


                 $("#btncalculo").click(function(){

                   var dataString = $('#consultarcalculo').serialize();

                    $.ajax({
                      url:"calculopagoslocal.php",
                      data : dataString,
                      type : 'POST',
                      dataType :'html',
                      success:function(resultado) {
                              $('#resultadoconsulta').html(resultado);
                      },error : function(xhr, status) {
                          if (xhr.status == 404) {

                             alert('Requested page not found [404]');

                            }
                      }
                         });
              });

              $("#btnasignado").click(function(){

                var dataString = $('#consultarasignado').serialize();

                 $.ajax({
                   url:"../reportes/codigosasignados.php",
                   data : dataString,
                   type : 'POST',
                   dataType :'html',
                   success:function(resultado) {
                           $('#resultadoconsulta').html(resultado);
                   },error : function(xhr, status) {
                       if (xhr.status == 404) {

                          alert('Requested page not found [404]');

                         }
                   }
                      });
           });
         });

     </script>

</body>
</html>
