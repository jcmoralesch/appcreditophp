<?php
 session_start();
 if(!isset($_SESSION["usuario"])){
      header("Location:../index.html");
  }

include 'plantilla.php';
include '../clases/tipocredito.php';

$fecha1=$_POST['fecha1'];
$fecha2=$_POST['fecha2'];

$pago= new TipoCredito();

$array_cantidad=$pago->colocacion($fecha1,$fecha2);


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');

$pdf->SetFillColor(232,232,232);

$pdf->SetFont('Arial','B',9);
$pdf->Cell(32,6,utf8_decode('Ruta'),1,0,'C',1);
$pdf->Cell(32,6,'Cantidad',1,0,'C',1);
$pdf->Cell(24,6,utf8_decode('No. créditos'),1,0,'C',1);
$pdf->Cell(32,6,utf8_decode('Fecha'),1,1,'C',1);


$total=0;
foreach ($array_cantidad as $elemento) {
	$pdf->Cell(32,6,$elemento['id_ruta'],'B'.'L',0,'C');
	$pdf->Cell(32,6,$elemento['sum(R.monto)'],'B',0,'C');
	$pdf->Cell(24,6,$elemento['count(R.monto)'],'B',0,'C');
	$pdf->Cell(32,6,$elemento["date_format(R.fecha_registro,'%d/%m/%Y')"],'B'.'R',1,'C');
    $total=$total+$elemento['sum(R.monto)'];
}
$pdf->Cell(50,6,'TOTAL COLOCADO: '.$total,0,0,'C');
//$pdf->Output('D','Reporte_clientes.pdf');
$pdf->Output();

?>