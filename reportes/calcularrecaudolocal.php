<?php session_start(); if(!isset($_SESSION["usuario"])){ header("Location:../index.html");}

include 'plantilla.php';
include '../clases/empleado.php';

$fecha1=$_POST['fecha1'];
$fecha2=$_POST['fecha2'];
$ruta=$_POST['ruta'];

$recaudo= new Empleado();

$array_recaudo=$recaudo->calcularRecaudo($fecha1,$fecha2,$ruta);

$datos='';


$datos.='<div id="espacio"></div>
         <div class="container-fluid">
         <div class="row">
         <div class="col"></div>
         <div class="col-lg-8">
<div class="p-1 mb-1 bg-info text-white text-lg-center">RECAUDO DE '.$ruta.' DEL '.$fecha1.' AL '.$fecha2.'   </div>
<table class="table table-sm table-striped table-hover">
  <thead class="text-center">
    <tr>
      <th scope="col">Cantidad cobrada</th>
      <th scope="col">Fecha de cobro</th>
    </tr>
  </thead>
  <tbody class="text-center">';


$total=0;


foreach ($array_recaudo as $elemento) {
	$datos.= '<tr>
      <th scope="row">'.$elemento['cant'].'</th>
      <td>'.$elemento['fech'].'</td>
    </tr>';

	$total=$total+$elemento['cant'];
}
$porcent=$total*0.025;
$sum=$porcent+$total;

$datos.=' </tbody>
</table>';

echo $datos;
echo '<p class="h6">TOTAL COBRADO: ' .$total.'&nbsp; &nbsp; &nbsp; Porcentaje '. $porcent.'</p></div>
<div class="col"></div></div></div>';

?>