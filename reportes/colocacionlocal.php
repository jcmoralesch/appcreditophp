<?php session_start(); if(!isset($_SESSION["usuario"])){ header("Location:../index.html");}

include 'plantilla.php';
include '../clases/tipocredito.php';

$fecha1=$_POST['fecha1'];
$fecha2=$_POST['fecha2'];

$pago= new TipoCredito();

$array_cantidad=$pago->colocacion($fecha1,$fecha2);


$datos='';


$datos.='
<div class="p-1 mb-1 bg-success text-white text-lg-center">COLOCACIÓN  DEL  '.$fecha1.' AL '.$fecha2.'   </div>
<table class="table table-sm table-striped table-hover">
  <thead>
    <tr>
      <th scope="col">Ruta</th>
      <th scope="col">Cantidad</th>
      <th scope="col">No. créditos</th>
      <th scope="col">Fecha</th>
    </tr>
  </thead>
  <tbody>';


$total=0;
foreach ($array_cantidad as $elemento) {
	
    $datos.= '<tr>
      <th scope="row">'.$elemento['id_ruta'].'</th>
      <td>'.$elemento['sum(R.monto)'].'</td>
      <td>'.$elemento['count(R.monto)'].'</td>
      <td>'.$elemento["date_format(R.fecha_registro,'%d/%m/%Y')"].'</td>
    </tr>';


    $total=$total+$elemento['sum(R.monto)'];
}
$datos.=' </tbody>
</table>';

echo $datos;
echo '<p class="h6">TOTAL COLOCADO: ' .$total.'</p>';

?>