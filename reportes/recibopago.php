<?php
 session_start();
 if(!isset($_SESSION["usuario"])){
      header("Location:../index.html");
  }

include 'plantilla.php';
include '../clases/empleado.php';

$id=$_POST['id'];


$recibo= new Empleado();

$array_recibo=$recibo->calcularPago($id);


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');


foreach ($array_recibo as $elemento) {
	$nombre=$elemento['nombre'];
    $apellido=$elemento['apellido'];
    $ruta=$elemento['id_ruta'];
    $viatico=$elemento['viaticos'];
    $sueldob=$elemento['sueldo_base'];
    $recaudo=$elemento['recaudo'];
    $porcentaje=$elemento['porcentaje'];
    $sueldo=$elemento['sueldo_total'];
    $fecha=$elemento['fecha'];
}

$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(20,10,utf8_decode('Nombre: '),0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(60,10,$nombre,0,0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(20,10,'Apellido: ',0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(60,10,$apellido,0,1,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(15,6,'Ruta: ',0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20,6,$ruta,0,1,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(20,6,'Viaticos: ',0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(15,6,$viatico,0,0,'C');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,6,'Sueldo base: ',0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(10,6,$sueldob,0,1,'C');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(20,6,'Recaudo: ',0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20,6,$recaudo,0,0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(20,6,'Porcentaje: ',0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(15,6,$porcentaje,0,0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(20,6,'Total sueldo: ',0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(15,6,$sueldo ,0,0,'C');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(20,6,'Fecha: ',0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->Cell(15,6,$fecha,0,1,'C');

$pdf->Output();

?>