use controlcreditos;

drop procedure verificarMora;
DELIMITER $$
CREATE FUNCTION mora_generada(idCredito INT) RETURNS float(8,2) 
begin 
  declare moraGenerada float(8,2); 
  select mora_generada into moraGenerada from mora where id_credito=idCredito; 
  return moraGenerada; 
 END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION cuota(idCredito INT) RETURNS float(8,2) 
begin 
  declare c float(8,2); 
  select cuota into c from credito where id_credito=idCredito; 
  return c; 
 END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION total_pagado(idCredito INT) RETURNS float(8,2) 
begin 
  declare tp float(8,2); 
  select total_pagado into tp from estado_cuenta where id_credito=idCredito; 
  return tp; 
 END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION pagos_realizados(idCredito INT) returns int 
begin 
  declare pr int; 
  select pagos_realizados into pr from estado_cuenta where id_credito=idCredito; 
  return pr; 
 END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION saldo(idCredito INT) returns float(8,2)
begin 
  declare s int; 
  select saldo into s from estado_cuenta where id_credito=idCredito; 
  return s; 
 END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION pagos(idCredito INT) returns int
begin 
  declare p int; 
  select pagos into p from estado_cuenta where id_credito=idCredito; 
  return p; 
 END$$
DELIMITER ;


DELIMITER $$
CREATE FUNCTION adelantado(idCredito INT) returns int
begin 
  declare a int; 
  select adelantado into a from mora where id_credito=idCredito; 
  return a; 
 END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION dias_mora(idCredito INT) returns int
begin 
  declare dm int; 
  select dias_mora into dm from mora where id_credito=idCredito; 
  return dm; 
 END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION monto(idCredito INT) returns float(8,2)
begin 
  declare m float(8,2); 
  select monto into m from credito where id_credito=idCredito; 
  return m; 
 END$$
DELIMITER ;


DELIMITER $$
CREATE FUNCTION miniDeposito(idCredito INT) returns float(8,2)
begin 
  declare md float(8,2); 
  select miniDeposito into md from mora where id_credito=idCredito; 
  return md; 
 END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION tipo_credito(idCredito INT) returns int
begin 
  declare tipo int; 
  declare var varchar(15);
  select id_tipo_credito into var from credito where id_credito=idCredito; 
  if var="SEMANAL" then
     set tipo=0;
  elseif var="DIARIO" then
	  set tipo=1;
  end if;  
  return tipo;
 END$$
DELIMITER ;






drop procedure registrarDeposito;

  DELIMITER $$
  CREATE  PROCEDURE `registrarDeposito` (IN `inIdCredito` INT, IN `inCantidad` FLOAT(8.2), IN `inMora` FLOAT)  begin
  declare restar float;
  declare menosPago int;
  declare sumarCuotas float;
  declare sumarPagos int;
  DECLARE traercuota float;
  declare calcularNoPagos float;
  declare verificarMora int;
  declare verifica_fecha_pago int;
  declare consultarMiniDeposito float;
  declare sumarMiniDeposito float;
  declare obtenerRestante float;
  declare datosconMod float;
  declare fechaReg date;
  declare id_tipo_credito int;
  declare restar_noPagos int;
   
  set id_tipo_credito =tipo_credito(inIdCredito);
  set verifica_fecha_pago=obtener_fecha_pago(inIdCredito);
  set traercuota=cuota(inIdCredito);
  set calcularNoPagos=(select floor(inCantidad/traercuota));
  set sumarCuotas=total_pagado(inIdCredito)+inCantidad;
  set sumarPagos=pagos_realizados(inIdCredito)+calcularNoPagos; 
  set restar=saldo(inIdCredito)-inCantidad;
  set menosPago=pagos(inIdCredito)-calcularNoPagos;
  set datosconMod=(select (inCantidad % traercuota));
  set consultarMiniDeposito=miniDeposito(inIdCredito);
  set sumarMiniDeposito=(consultarMiniDeposito + datosconMod);
  
  update mora set mora_generada=mora_generada-inMora where id_credito=inIdCredito;
  
  if id_tipo_credito =0 then
     if verifica_fecha_pago=0 then
    
        if calcularNoPagos > 1 then      
             set verificarMora=dias_mora(inIdCredito);  
             update estado_cuenta set saldo=restar,pagos=menosPago,total_pagado=sumarCuotas,pagos_realizados=sumarPagos,estado_pago='Pendiente'
             where id_credito=inIdCredito;
             
             if verificarMora > calcularNoPagos then
                   update mora set  total_de_mora=total_de_mora-(calcularNoPagos*traercuota),dias_mora=dias_mora-calcularNopagos where id_credito=inIdCredito;      
             elseif verificarMora < calcularNoPagos then         
                   if verificarMora=0 then
                      update mora set adelantado=adelantado+calcularNoPagos,total_adelantado=
                      total_adelantado+(traercuota*calcularNoPagos) where id_credito=inIdCredito; 
                   else
                      update mora set dias_mora=0,total_de_mora=0.00, adelantado=calcularNoPagos-verificarMora,total_adelantado=
                      ((calcularNoPagos-verificarMora)*traercuota) where id_credito=inIdCredito;                    
                   end if;             
               elseif verificarMora = calcularNoPagos then
                   update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;
        
               end if;
            
             if datosconMod >0.00 then                     
                 set verificarMora=dias_mora(inIdCredito);
                            
                 if(sumarMiniDeposito<traercuota) then
                     update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
                 elseif(sumarMiniDeposito=traercuota) then
                     update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                     update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados +1,estado_pago='Pendiente'
                     where id_credito=inIdCredito;   
                      
                      if verificarMora=1 then
                          update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;              
                      elseif verificarMora>1 then
                          update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                      elseif verificarMora=0 then  
                          update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;
                                                 
                      end if;                        
                            
                 elseif(sumarMiniDeposito>traercuota)then
                      set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                      update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                      update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='Pendiente'
                      where id_credito=inIdCredito;
 
                      if verificarMora=1 then
                           update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;              
                      elseif verificarMora>1 then
                           update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito;                                
                      elseif verificarMora=0 then  
                            update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;
                      end if;                                                 
                 end if;                                            
            end if;
                                  
         elseif calcularNoPagos=1 then
               set verificarMora=dias_mora(inIdCredito);
               update estado_cuenta set saldo=restar,pagos=menosPago,total_pagado=sumarCuotas,pagos_realizados=sumarPagos,estado_pago='Pendiente'
               where id_credito=inIdCredito; 
               
               if verificarMora=1 then
                    update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;                 
               elseif verificarMora>1 then       
                    update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito;                                  
               elseif verificarMora=0 then 
                    update mora set adelantado=adelantado+calcularNoPagos,total_adelantado=total_adelantado+(traercuota*calcularNoPagos) where id_credito=inIdCredito;                                        
               end if;
                  
              if datosconMod >0.00 then                     
                  
                  if(sumarMiniDeposito<traercuota) then
                        update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
                  elseif(sumarMiniDeposito=traercuota) then
                        update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                        update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='Pendiente'
                        where id_credito=inIdCredito;
                        
                        set verificarMora=dias_mora(inIdCredito);
                         if verificarMora=1 then
                             update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;          
                         elseif verificarMora>1 then
                             update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                             
                         elseif verificarMora=0 then  
                             update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;     
                        end if;                        
                        
                   elseif(sumarMiniDeposito>traercuota)then
                        set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                        update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                         update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='Pendiente'
                         where id_credito=inIdCredito;
                        
                         set verificarMora=dias_mora(inIdCredito);
                         if verificarMora=1 then
                              update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;           
                         elseif verificarMora>1 then
                              update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito;                           
                         elseif verificarMora=0 then  
                              update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;     
                        end if;                                                
                   end if;                                         
                end if; 
                          
         elseif calcularNoPagos < 1 then
            update estado_cuenta set saldo=restar,total_pagado=sumarCuotas,estado_pago='PAGADO' where id_credito=inIdCredito;
            
            if(sumarMiniDeposito<traercuota) then
                update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito; 
           elseif(sumarMiniDeposito=traercuota) then
                update mora set miniDeposito=0.00 where id_credito=inIdCredito;          
                update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1, estado_pago='Pendiente' where id_credito=inIdCredito;
                set verificarMora=dias_mora(inIdCredito); 
                if verificarMora=1 then
                    update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito; 
                                
                elseif verificarMora>1 then                                           
                    update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito;                          
                elseif verificarMora=0 then 
                      update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;          
                end if;                         
                        
            elseif(sumarMiniDeposito>traercuota)then
                    set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                    update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;   
                    update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1, estado_pago='Pendiente' where id_credito=inIdCredito;
                    set verificarMora=dias_mora(inIdCredito);
                    
                    if verificarMora=1 then
                        update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;           
                    elseif verificarMora>1 then  
                        update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                           
                   elseif verificarMora=0 then 
                        update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;
                        update estado_cuenta set estado_pago='Pendiente' where id_credito=inIdCredito;                 
                  end if;                                              
             end if;                                
      end if;
          
  elseif verifica_fecha_pago=1 then
            if calcularNoPagos > 1 then
               set verificarMora=dias_mora(inIdCredito); 
               update estado_cuenta set saldo=restar,pagos=menosPago,total_pagado=sumarCuotas,pagos_realizados=sumarPagos,estado_pago='PAGADO'
               where id_credito=inIdCredito;   
           
               set restar_noPagos=calcularNoPagos-1;        
               
               if verificarMora > restar_noPagos then
                   update mora set  total_de_mora=total_de_mora-(restar_noPagos*traercuota),dias_mora=dias_mora-restar_noPagos where id_credito=inIdCredito;      
               elseif verificarMora < restar_noPagos then         
                   if verificarMora=0 then
                      update mora set adelantado=adelantado+restar_noPagos,total_adelantado=
                      total_adelantado+(traercuota*restar_noPagos) where id_credito=inIdCredito;  
                   else
                      update mora set dias_mora=0,total_de_mora=0.00, adelantado=restar_noPagos-verificarMora,total_adelantado=
                      ((restar_noPagos-verificarMora)*traercuota) where id_credito=inIdCredito;           
                   end if;             
               elseif verificarMora = restar_noPagos then
                   update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;
               end if;
          
                if datosconMod >0.00 then 
                     set verificarMora=dias_mora(inIdCredito);
                                        
                     if(sumarMiniDeposito<traercuota) then
                        update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
                     elseif(sumarMiniDeposito=traercuota) then
                        update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                        update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados +1,estado_pago='PAGADO'
                        where id_credito=inIdCredito;
    
                         if verificarMora=1 then
                             update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;       
                         elseif verificarMora>1 then
                             update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito;  
                        elseif verificarMora=0 then
                            update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;                           
                        end if;                        
                        
                   elseif(sumarMiniDeposito>traercuota)then
                        set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                        update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                        update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                        where id_credito=inIdCredito;
                        
                         if verificarMora=1 then
                             update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;           
                         elseif verificarMora>1 then
                             update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                         elseif verificarMora=0 then
                             update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;     
                        end if;                                               
                   end if;                                         
               end if;
                   
           elseif calcularNoPagos=1 then
                   set verificarMora=dias_mora(inIdCredito);
                   update estado_cuenta set saldo=restar,pagos=menosPago,total_pagado=sumarCuotas,pagos_realizados=sumarPagos,
                   estado_pago='PAGADO' where id_credito=inIdCredito;
                  
                  if datosconMod >0.00 then 
                  
                     set verificarMora=dias_mora(inIdCredito);                   
           
                     if(sumarMiniDeposito<traercuota) then
                        update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
                        
                     elseif(sumarMiniDeposito=traercuota) then
                        update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                        update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                        where id_credito=inIdCredito;
                        
                         if verificarMora=1 then
                             update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;          
                         elseif verificarMora>1 then
                             update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                             update estado_cuenta set estado_pago='Pendiente' where id_credito=inIdCredito;
                         elseif verificarMora=0 then
                             update mora set adelantado=adelantado+1, total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;          
                        end if;                        
                        
                   elseif(sumarMiniDeposito>traercuota)then
                        set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                        update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                        update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                        where id_credito=inIdCredito;
                        
                         if verificarMora=1 then
                             update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;          
                         elseif verificarMora>1 then
                             update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito;  
                         elseif verificarMora=0 then
                             update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+
                             traercuota where id_credito=inIdCredito;         
                        end if;                                          
                   end if;                                       
                end if;                     
                  
        elseif calcularNoPagos < 1 then

                 set verificarMora=dias_mora(inIdCredito);
                 update estado_cuenta set saldo=restar,total_pagado=sumarCuotas,estado_pago='PAGADO' where id_credito=inIdCredito;
            
               if(sumarMiniDeposito<traercuota) then
                   update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;     
               elseif(sumarMiniDeposito=traercuota) then
                   update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                   update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO' where id_credito=inIdCredito;                                                     
               elseif(sumarMiniDeposito>traercuota)then
                    set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                    update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                    update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1 where id_credito=inIdCredito;                                                   
             end if;                
          end if;
    end if;
 
  elseif id_tipo_credito =1 then
     if verifica_fecha_pago=0 then 
        if calcularNoPagos > 1 then
            set verificarMora=dias_mora(inIdCredito);  
            update estado_cuenta set saldo=restar,pagos=menosPago,total_pagado=sumarCuotas,pagos_realizados=sumarPagos,estado_pago='Pendiente'
            where id_credito=inIdCredito;
            
            if verificarMora > calcularNoPagos then
                 update mora set  total_de_mora=total_de_mora-(calcularNoPagos*traercuota),dias_mora=dias_mora-calcularNopagos where id_credito=inIdCredito;      
            elseif verificarMora < calcularNoPagos then   
                 set fechaReg=(select fecha_registro from credito where id_credito=inIdCredito); 
                      
                 if fechaReg=current_date()then
                      update mora set adelantado=adelantado+calcularNoPagos,total_adelantado=
                      total_adelantado+(traercuota*calcularNoPagos) where id_credito=inIdCredito;                     
                 end if;             
               elseif verificarMora = calcularNoPagos then
                   update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;
        
               end if;
            
            if datosconMod >0.00 then   
                set verificarMora=dias_mora(inIdCredito);                  
                if(sumarMiniDeposito<traercuota) then
                      update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
                elseif(sumarMiniDeposito=traercuota) then
                    update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                    update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1 where id_credito=inIdCredito;
                    
                    if verificarMora=1 then
                         update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;      
                    elseif verificarMora>1 then
                         update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                    end if;                        
                            
                elseif(sumarMiniDeposito>traercuota)then
                    set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                    update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                    update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1 where id_credito=inIdCredito;
                    
                    if verificarMora=1 then
                         update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;        
                    elseif verificarMora>1 then
                         update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                    end if;
                end if;
           end if;
                                  
        elseif calcularNoPagos=1 then
            set verificarMora=dias_mora(inIdCredito);
            update estado_cuenta set saldo=restar,pagos=menosPago,total_pagado=sumarCuotas,pagos_realizados=sumarPagos,estado_pago='Pendiente'
            where id_credito=inIdCredito;
            
            if verificarMora=1 then
                update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;      
            elseif verificarMora>1 then
                update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
            elseif verificarMora=0 then 
                set fechaReg=(select fecha_registro from credito where id_credito=inIdCredito);
                if fechaReg=current_date()then
                    update mora set adelantado=calcularNoPagos, total_adelantado=inCantidad where id_credito=inIdCredito;
                end if;
            end if;
                  
            if datosconMod >0.00 then  
                set verificarMora=dias_mora(inIdCredito);
                                   
                if(sumarMiniDeposito<traercuota) then
                    update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
                elseif(sumarMiniDeposito=traercuota) then
                    update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                    update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1 where id_credito=inIdCredito;
                        
                    if verificarMora=1 then
                         update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;       
                    elseif verificarMora>1 then
                         update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                    end if;                        
                        
                elseif(sumarMiniDeposito>traercuota)then
                    set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                    update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                    update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1 where id_credito=inIdCredito;
  
                    if verificarMora=1 then
                        update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;        
                    elseif verificarMora>1 then
                        update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                    end if;
                end if;
            end if; 
               
        elseif calcularNoPagos < 1 then
            set verificarMora=dias_mora(inIdCredito);
            update estado_cuenta set saldo=restar,total_pagado=sumarCuotas where id_credito=inIdCredito;
            
            if(sumarMiniDeposito<traercuota) then
                set fechaReg=(select fecha_registro from credito where id_credito=inIdCredito);
                if fechaReg=current_date()then
                    update estado_cuenta set estado_pago='PAGADO' where id_credito=inIdCredito;
                end if;
                 update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;     
            elseif(sumarMiniDeposito=traercuota) then
                 update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                 update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1, estado_pago='Pendiente' 
                 where id_credito=inIdCredito;
                 
                 if verificarMora=1 then
                     update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;            
                 elseif verificarMora>1 then                     
                     update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                 end if;                         
                        
            elseif(sumarMiniDeposito>traercuota)then
                 set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                 update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;    
                 update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='Pendiente'
                 where id_credito=inIdCredito;
                 
                 if verificarMora=1 then
                     update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;                 
                 elseif verificarMora>1 then  
                     update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito;        
                 end if; 
             end if;    
        end if;
          
  elseif verifica_fecha_pago=1 then
       if calcularNoPagos > 1 then 
           set verificarMora=dias_mora(inIdCredito);
           update estado_cuenta set saldo= restar,pagos=menosPago,total_pagado=sumarCuotas,pagos_realizados=sumarPagos,
           estado_pago='PAGADO' where id_credito=inIdCredito;

           set restar_noPagos=calcularNoPagos-1;        
      
           if verificarMora > restar_noPagos then
               update mora set  total_de_mora=total_de_mora-(restar_noPagos*traercuota),dias_mora=dias_mora-restar_noPagos where id_credito=inIdCredito;      
           elseif verificarMora < restar_noPagos then         
               if verificarMora=0 then
                  update mora set adelantado=adelantado+restar_noPagos,total_adelantado=
                  total_adelantado+(traercuota*restar_noPagos) where id_credito=inIdCredito;  
               else
                  update mora set dias_mora=0,total_de_mora=0.00, adelantado=restar_noPagos-verificarMora,total_adelantado=
                  ((restar_noPagos-verificarMora)*traercuota) where id_credito=inIdCredito;           
               end if;             
           elseif verificarMora = restar_noPagos then
               update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;
           end if;
      
           if datosconMod >0.00 then    
              set verificarMora=dias_mora(inIdCredito);                  
              if(sumarMiniDeposito<traercuota) then
                  update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
              elseif(sumarMiniDeposito=traercuota) then
                  update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                   update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                   where id_credito=inIdCredito;
                        
                  if verificarMora=1 then
                       update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;              
                  elseif verificarMora>1 then
                       update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                  elseif verificarMora=0 then
                       update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;                                 
                  end if;                        
                        
              elseif(sumarMiniDeposito>traercuota)then
                   set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                   update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                   update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                   where id_credito=inIdCredito;

                   if verificarMora=1 then
                         update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;           
                  elseif verificarMora>1 then
                         update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                  elseif verificarMora=0 then
                         update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;                                 
                 end if;
             end if;
          end if;
                   
       elseif calcularNoPagos=1 then
            set verificarMora=dias_mora(inIdCredito);
            update estado_cuenta set saldo=restar,pagos=menosPago,total_pagado=sumarCuotas,pagos_realizados=sumarPagos,
            estado_pago='PAGADO' where id_credito=inIdCredito;
                  
           if datosconMod >0.00 then 
               set verificarMora=dias_mora(inIdCredito);
                                   
               if(sumarMiniDeposito<traercuota) then
                    update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
               elseif(sumarMiniDeposito=traercuota) then
                    update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                    update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                    where id_credito=inIdCredito;

                    if verificarMora=1 then
                        update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;       
                    elseif verificarMora>1 then
                        update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito; 
                    elseif verificarMora=0 then 
                        update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito; 
                    end if;                        
                        
               elseif(sumarMiniDeposito>traercuota)then
                     set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                     update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                     update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                     where id_credito=inIdCredito;
                     
                     if verificarMora=1 then
                          update mora set dias_mora=0,total_de_mora=0.00 where id_credito=inIdCredito;        
                     elseif verificarMora>1 then
                          update mora set dias_mora=dias_mora-1,total_de_mora=total_de_mora-traercuota where id_credito=inIdCredito;
                     elseif verificarMora=0 then
                          update mora set adelantado=adelantado+1,total_adelantado=total_adelantado+traercuota where id_credito=inIdCredito;    
                     end if;
                 end if;
             end if;                     
                  
         elseif calcularNoPagos < 1 then
              update estado_cuenta set saldo=restar,total_pagado=sumarCuotas,estado_pago='PAGADO' where id_credito=inIdCredito;
              set verificarMora=dias_mora(inIdCredito);
              
              if(sumarMiniDeposito<traercuota) then
                   update mora set miniDeposito=sumarMiniDeposito where id_credito=inIdCredito;
              elseif(sumarMiniDeposito=traercuota) then
                   update mora set miniDeposito=0.00 where id_credito=inIdCredito;
                   update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                   where id_credito=inIdCredito;                                            
             elseif(sumarMiniDeposito>traercuota)then
                    set obtenerRestante=(select (sumarMiniDeposito % traercuota));                        
                    update mora set miniDeposito=obtenerRestante where id_credito=inIdCredito;
                    update estado_cuenta set pagos=pagos-1,pagos_realizados=pagos_realizados+1,estado_pago='PAGADO'
                    where id_credito=inIdCredito;
                 end if;
             end if;
       end if;  
  end if;
  
  INSERT INTO deposito(cantidad,mora,fecha_deposito,hora,id_credito) 
  VALUES (inCantidad,inMora,current_date(),DATE_SUB(NOW(),INTERVAL 1 HOUR),inIdCredito);
  
  IF (saldo(inIdCredito)=0.00 and mora_generada(inIdCredito)=0.00) THEN
       UPDATE credito SET status='C' where id_credito=inIdCredito;
       UPDATE estado_cuenta SET status='C' where id_credito=inIdCredito;
       call registrar_record_cliente(inIdCredito);
       delete from fecha_pagos where id_credito=inIdCredito;
       delete from r_estado_cuenta where id_credito=inIdCredito;
       delete from r_mora where id_credito=inIdCredito; 
       update cliente set status='I' where identificacion=(select identificacion from credito where id_credito=inIdCredito);    
  END IF;
  
end$$
DELIMITER ;





drop procedure actualizaFechaFestivo;
DELIMITER $$
CREATE PROCEDURE `actualizaFechaFestivo` (IN `inFechaFestivo` DATE)  begin 
     
   declare V_IDCREDITO int;
   declare V_IDFECHA int;
   declare vencimiento date;
   declare sumarFecha date;
   declare sumarFechaSemanal date;
   declare V_HAY_REGISTROS boolean default true;

  
         declare C_CREDITO cursor for 
                         select id_credito,id_pago from fecha_pagos where fecha=inFechaFestivo;
         declare continue handler for not found
                 set V_HAY_REGISTROS = false;
      
         open C_CREDITO;   
         fetch C_CREDITO into V_IDCREDITO,V_IDFECHA;
           
         while V_HAY_REGISTROS do
                   set vencimiento=(select fecha_vencimiento from credito where id_credito=V_IDCREDITO);
                   set sumarFecha=(select * from fecha where fecha>vencimiento limit 1);
                   set sumarFechaSemanal=(select * from fecha where fecha>inFechaFestivo limit 1);
       
                   if(tipo_credito(V_IDCREDITO)=0) then
                      update fecha_pagos set fecha=sumarFechaSemanal where  id_pago=V_IDFECHA;
                   else
                      update fecha_pagos set fecha=sumarFecha where  id_pago=V_IDFECHA;
                      update credito set fecha_vencimiento=sumarFecha where id_credito=V_IDCREDITO;
                   end if;                 
             fetch C_CREDITO into V_IDCREDITO,V_IDFECHA;
         
         end while;
         close C_CREDITO;   
end$$
