<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />

<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">
<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">

<title>Tipos de pago</title>
<style type="text/css">

input{
border: none;
}
</style> 
</head>

<body id="pag">
     <?php
         require_once "../clases/tipocredito.php"; 
         $tcredito = new TipoCredito();
      ?>
   
           <header>
              <img src="../formularios/logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>

           <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="cliente.php">Clientes</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportegestoresruta/plantilllareporte.php">Clientes</a>
                    <a class="dropdown-item" href="clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="clientesenmora.php">En mora</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="../formularios/cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Inicio</li>
          </ol>
        </nav>

        <div class="container">
                <div class="row justify-content-md-center">
                   <div class="col-lg-8">  
                            <div id="espacio2">
                            <div class="table-responsive"> 
                            <table class="table table-bordered table-hover table-condensed table-striped table-sm">
                                <thead bgcolor="#58ACFA">
                                  <th>Id. tipo pago</th>
                                  <th>Nombre</th>
                                  <th>No. de pagos</th>
                                  <th>Interés </th>
                                  <th>Plazo</th>
                                </thead>

                                   <?php 
                                              $array_credito=$tcredito->consultarTPago(); 

                                             foreach($array_credito as $elemento){
                                                  echo "<tr><td>".$elemento['id_tipo']."</td><td>";
                                                  echo $elemento['nombre_tipo']."</td><td>";
                                                  echo $elemento['no_pago']."</td><td>";
                                                  echo $elemento['interes']."</td><td>";
                                                  echo $elemento['plazo']."</td></tr>";

                                             }
                                      ?>
                                      

                             </table>
                             </div> 
                             </div>

                     </div>
                 </div>
                 </div>
          
           <footer>
           	
           </footer>   

            <script src="../view/js/jquery-3.2.1.min.js"></script>
           <script src="../view/js/popper.min.js"></script>
           <script src="../view/js/bootstrap.min.js"></script>    
</body>	
</html>