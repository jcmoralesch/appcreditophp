<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="../estilopagina.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<title>Area de reportes</title>
</head>

<body>
      <?php
          require_once "../clases/tipocredito.php"; 
          $tCredito = new TipoCredito();
      ?>
     <div id="contenedor">
           <header>
              <h1>Reporte por ruta</h1>

              <h3>Bienvenido</h3>
              <?php
                echo "Buen dia: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>

           <nav>   
                <ul id="menuHorizontal"> 
                     </li>
                     <li><a href="../cerrar.php">Salir</a> 
                              
                     </li>
                
                </ul>

           </nav>
            <div id="porruta">
           
            
                     <form  role="form" action="../reportegestoresruta/reportegestor123e.php" method="post">
                       <table>
                       <tr>
                       <td><div class="form-group">
                            <label style="width: 150px;">Genere su reporte:</label></td>
                            
                            <td style="width: 150px;"><select name="tipocredito" class="form-control input-sm">
                                                 <?php
                                                      $array_ruta=$tCredito->consultarTCredito();

                                                    foreach($array_ruta as $elemento){
                                                       echo"<option>";
                                                       echo $elemento['id_credito'];
                                                       echo"</option>";
                                                      }
                                                   ?>


                                            </select></td>
                              <td><input type="submit" name="registrar" value="Generar" class="btn btn-info btn-sm"></td>               
                        </div>
                        </tr>

                        </table>

                     </form>

           </div>

           <footer>
           	    
           </footer>
     </div>       
</body>	
</html>
