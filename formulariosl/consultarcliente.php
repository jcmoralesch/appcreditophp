<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../estilopagina.css" rel="stylesheet" type="text/css">
<link href="../bootstrap.css" rel="stylesheet" type="text/css">
<title>Clientes activos</title>
</head>

<body>
     <?php
         require_once "../clases/cliente.php"; 
         $cliente = new Cliente();

         $id=$_SESSION['identificacion'];
         $array_id=$cliente->consultarRutaEmpleado($id);
         foreach($array_id as $elem){
              $ruta=$elem['id_ruta'];      
          }

          $tam=strlen($ruta);
          $pos=substr($ruta, 0,1); 

          if($tam==3){
             $pos1=substr($ruta,-2); 
             $array_ruta=$cliente->mostrarClientesPorRuta($pos,$pos1);

          }
          else if($tam==4){
            $pos2=substr($ruta,-3);
            $array_ruta=$cliente->mostrarClientesPorRuta($pos,$pos2);
          }
      ?>
    <div id="contenedor"> 

           <header>
              <h1>Listado general de clientes</h1>
              <img src="logo.gif">
              <?php
                echo "Usuario: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>
             
           <nav>
                <ul id="menuHorizontal">

                     <li>Registros
                         <ul class="submenu">
                             <li><a href="cliente.php">Clientes</a></li>
                         </ul>
                     </li>

                     <li>Consultas
                         <ul class="submenu">
                             <li><a href="consultarcliente.php">Clientes</a></li>
                         </ul>    
                            
                     </li>

                     <li>Reportes
                            <ul class="submenu">
                             <li><a href="../reportegestoresruta/plantilllareporte.php">Clientes</a></li>
                         </ul>  
                     </li>

                     <li><a href="../cerrar.php">Salir</a> 
                              
                     </li>
                
                </ul>
           	

           </nav>
           <section id="clientes">  
                <table class="table table-bordered table-hover">
                    <thead>
                      <th>Ruta</th>
                      <th>Cliente</th>
                      <th>Capital</th>
                      <th>Interés</th>
                      <th>Cuotas</th>
                      <th>Cuota</th>
                      <th>Teléfono</th>
                      <th>FCH DSMBS</th>
                      <th>Ciclo</th>
                      <th>Total</th>
                      <th>Restante</th>
                      <th>CP</th>
                      <th>CF</th>
                    </thead>
                      

                       <?php 
                                  $array_cliente=$cliente->mostrarClientesActivos(); 
                                  $var=1;

                                 foreach($array_ruta as $elemento){
                                      echo"<tr><td>";
                                      echo $elemento['id_ruta'] . $var++."</td><td>";
                                      echo $elemento['cliente']."</td><td>";
                                      echo $elemento['monto']."</td><td>";
                                      echo $elemento['interes']."</td><td>";
                                      echo $elemento['cuotas']."</td><td>";
                                      echo $elemento['cuota']."</td><td>";
                                      echo $elemento['telefono']."</td><td>";
                                      echo $elemento['fecha']."</td><td>";
                                      echo $elemento['no_ciclo']."</td><td>";
                                      echo $elemento['total_pagado']."</td><td>";
                                      echo $elemento['saldo']."</td><td>";
                                      echo $elemento['pagos_realizados']."</td><td>";
                                      echo $elemento['pagos']."</td></tr>";


                                 }
                           ?>

                 </table>
                
           </section>
          
           <footer>
           	
           </footer>
    </div>       
</body>	
</html>