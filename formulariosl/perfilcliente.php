<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}

         require_once "../clases/cliente.php"; 
         $cliente = new Cliente();

         $identificacion=$_POST['cod'];
    
                
                           $array_cliente=$cliente->consultarPerfilCliente($identificacion); 

                                 foreach($array_cliente as $elemento){
                            
                                      $identificacion= $elemento['identificacion'];
                                      $nombre= $elemento['nombre'];
                                      $apellido= $elemento['apellido'];
                                      $direccion= $elemento['direccion'];
                                      $telefono=$elemento['telefono'];
                                      $fechareg= $elemento['fecharc'];
                                      $idruta= $elemento['id_ruta'];
                                      $nociclo= $elemento['no_ciclo'];
                                      $idcredito= $elemento['id_credito'];
                                      $monto= $elemento['monto'];
                                      $montototal=$elemento['monto_total'];
                                      $cuota= $elemento['cuota'];
                                      $fechainicio=$elemento['fechai'];
                                      $fechavenci=$elemento['fechav'];
                                      $tipocredito=$elemento['id_tipo_credito'];
                                      $tipopago=$elemento['id_tipo_pago'];
                                      $saldo=$elemento['saldo'];
                                      $pagos=$elemento['pagos'];
                                      $totalpagado=$elemento['total_pagado'];
                                      $pagosrealizados=$elemento['pagos_realizados'];
                                      $moragenerada=$elemento['mora_generada'];
                                      $diasmora=$elemento['dias_mora'];
                                      $totalmora=$elemento['total_de_mora'];
                                      $adelantado=$elemento['adelantado'];
                                      $totaladelantado=$elemento['total_adelantado'];
                                 }
                           ?>


     
                        <div class="p-1 mb-1 bg-info text-white text-lg-center">DATOS DE CLIENTE</div>
                        <p><label class="control-label">Código: </label><?php echo $identificacion;?></p>
                        <p><label class="control-label">Nombre: </label><?php echo $nombre;?></p>
                        <p><label class="control-label">Apellido: </label> <?php echo $apellido;?></p>
                        <p><label class="control-label">Dirección: </label><?php echo $direccion;?></p>
                        <p><label class="control-label">Telefono: </label> <?php echo $telefono;?></p>
                        <p><label class="control-label">Fecha registro: </label> <?php echo $fechareg;?></p>
                   

                    <div id="credito">
                        <div class="p-1 mb-1 bg-info text-white text-lg-center">DATOS DE CREDITO</div>
                        <div class="r"><label class="control-label">Ruta:</label> <?php echo $idruta;?></div>
                        <div class="r"><label class="control-label">Ciclo:</label> <?php echo $nociclo;?></div>
                        <div class="r"><label class="control-label">Id. credito:</label> <?php echo $idcredito;?></div>
                        <div class="r"><label class="control-label">Capital:</label> <?php echo $monto;?></div> 
                        <div class="r"><label class="control-label">Capital mas interés: </label><?php echo $montototal;?></div>
                        <div class="r"><label class="control-label">Cuotas:</label> <?php echo $cuota;?></div>
                        <div class="r"><label class="control-label">Fecha inicio de crédito:</label> <?php echo $fechainicio;?></div>
                        <div class="r"><label class="control-label">Fecha vencimiento crédito:</label><?php echo $fechavenci;?></div>
                        <div class="r"><label class="control-label">Tipo crédito:</label> <?php echo $tipocredito;?></div>
                        <div class="r"><label class="control-label">Tipo de pago: </label><?php echo $tipopago;?></div>
                        <div class="r"><label class="control-label">Saldo: </label><?php echo $saldo;?></div>
                        <div class="r"><label class="control-label">No. pagos: </label><?php echo $pagos;?></div>
                        <div class="r"><label class="control-label">Total pagado:</label> <?php echo $totalpagado;?></div>
                        <div class="r"><label class="control-label">No. de pagos realizados:</label><?php echo $pagosrealizados;?></div> 
                        <div class="r"><label class="control-label">Mora: </label><?php echo $moragenerada;?></div>
                        <div class="r"><label class="control-label">Días de atraso:</label> <?php echo $diasmora;?></div>
                        <div class="r"><label class="control-label">Total de cuotas acumuladas:</label> <?php echo $totalmora;?></div>
                        <div class="r"><label class="control-label">Días adelantados:</label> <?php echo $adelantado;?></div>
                        <div class="r"><label class="control-label">Total adelantado: </label><?php echo $totaladelantado;?></div>
                    </div> 