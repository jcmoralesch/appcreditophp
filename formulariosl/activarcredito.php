<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
	<title>Activar credito nuevo</title>

	<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../view/cssDT/dataTables.bootstrap.min.css">
    <link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">

</head>
<body id="pag">
      <?php
         require_once "../clases/tipocredito.php"; 
         $tCredito = new TipoCredito();

         $id=$_SESSION['identificacion'];
         $array_id=$tCredito->consultarRutaEmpleado($id);
         foreach($array_id as $elem){
              $ruta=$elem['id_ruta'];      
          }

          $tam=strlen($ruta);
          $pos=substr($ruta, 0,1); 
          $pos1="";

          if($tam==3){
             $pos1=substr($ruta,-1); 
          }
          else if($tam==4){
            $pos1=substr($ruta,-3);
          }
      ?>
	    <header>
              <img src="../formularios/logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"]."<br><br>";
              ?>
        </header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Otorgar crédito a clientes registrados</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item active">
                  <a class="nav-link" href="principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="cliente.php">Clientes</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportegestoresruta/plantilllareporte.php">Clientes</a>
                    <a class="dropdown-item" href="clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="clientesenmora.php">En mora</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="../formularios/cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>


        <input id="pos" name="pos" type="hidden" class="form-control" value="<?php echo $pos;?>">
       <input id="pos1" name="pos1" type="hidden" class="form-control" value="<?php echo $pos1;?>">

    <div class="container">
			<div class="row">
				<div id="cuadro2" class="col-sm-6 col-md-6 col-lg-6 ocultar" >
				   <div id="margenform" class="border border-info"> 
					<form class="form-horizontal" action="" method="POST">
						<input type="hidden" id="identificacion" name="identificacion" value="0">
						<input type="hidden" id="opcion" name="opcion" value="registrar">
						<div class="form-group">
							<label for="nombre" class="col-sm-4 control-label">Nombre</label>
							<div class="col-sm-8"><input id="nombre" name="nombre" type="text" class="form-control input-sm" required></div>				
						</div>
						<div class="form-group">
							<label for="apellido" class="col-sm-4 control-label">Apellido</label>
							<div class="col-sm-8"><input id="apellido" name="apellido" type="text" class="form-control input-sm" required ></div>
						</div>

						<div class="form-group">
							<label for="direccion" class="col-sm-4 control-label">Dirección</label>
							<div class="col-sm-8"><textarea id="direccion" name="direccion" rows="2" cols="30" maxlength="90" required class="form-control input-sm"></textarea></div>
						</div>

						<div class="form-group">
							<label for="telefono" class="col-sm-4 control-label">Teléfono</label>
							<div class="col-sm-8"><input id="telefono" name="telefono" type="text" class="form-control input-sm" required></div>
						</div>
						<div class="form-group">
							<label for="ruta" class="col-sm-4 control-label">Ruta</label>
							<div class="col-sm-8"><select name="ruta" id="ruta" required class="form-control input-sm">
		                                                 <?php
		                                                      $array_ruta=$tCredito->consultarRuta();

		                                                    foreach($array_ruta as $elemento){
		                                                       echo"<option>";
		                                                       echo $elemento['id_ruta'];
		                                                       echo"</option>";
		                                                      }
		                                                   ?>
		                                            </select></div>
						</div>
		                 <div class="form-group">
							<label for="credi" class="col-sm-4 control-label">Tipo de crédito:</label>
							<div class="col-sm-8"><select name="credi" id="credi" required class="form-control input-sm">
		                                                 <?php
		                                                      $array_tCredito=$tCredito->consultarTCredito();
		                                                              
		                                                       foreach($array_tCredito as $elemento){
		                                                            echo"<option>";
		                                                            echo $elemento['id_credito'];
		                                                            echo"</option>";
		                                                        } 
		                                                  ?>
		                                            </select></div>
						</div>
						<div class="form-group">
							<label for="pago" class="col-sm-4 control-label">Tipo de pago:</label>
							<div class="col-sm-8"><select name="pago" id="pago" required class="form-control input-sm">
		                                                 <?php
		                                                      $array_tPago=$tCredito->consultarTPago();

		                                                      foreach($array_tPago as $elemento){
		                                                          echo"<option>"; 
		                                                          echo $elemento['nombre_tipo'];
		                                                          echo"</option>";
		                                                      }
		                                                ?>
		                                            </select></div>
						</div>
						<div class="form-group">
							<label for="cantidad" class="col-sm-4 control-label">Cantidad otorgada</label>
							<div class="col-sm-8"><input id="cantidad" name="cantidad" type="text" class="form-control input-sm" placeholder="Cantidad sin comas (1000.00)" required ></div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-1 col-sm-11">
								<input id="" type="submit" class="btn btn-success btn-md btn-block" value="Otorgar crédito">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-1 col-sm-11">
								<input id="btn_listar" type="button" class="btn btn-info btn-md btn-block" value="Listar">
							</div>
						</div>
					</form>
					</div>
					<div class="col-sm-offset-2 col-sm-8">
						<!--<p class="mensaje"></p> -->
					</div>
					
				</div>
			</div>
	</div>

	<div class="container">
			<div class="row">
				<div id="cuadro1" class="col-sm-12 col-md-12 col-lg-12">
					<div class="col-sm-offset-2 col-sm-8">
						<h3 class="text-center"> <small class="mensaje"></small></h3>
					</div>
					<div class="table-responsive col-sm-12">		
						<table id="dt_cliente" class="table table-bordered table-hover table-condensed stripe" cellspacing="0" width="100%">
							<thead bgcolor="#58ACFA">
								<tr>
									  <th >Nombre</th>
		                              <th>Apellido</th>
				                      <th style="width: 400px;">Dirección</th>
				                      <th>Teléfono</th>
				                      <th></th>
		                              <th colspan="2"></th>											
								</tr>
							</thead>					
						</table>
					</div>			
				</div>		
			</div>
	</div>
	
	<script src="../view/js/jquery-3.2.1.min.js"></script>
    <script src="../view/js/popper.min.js"></script>
    <script src="../view/js/bootstrap.min.js"></script>
	<script src="../view/js/jquery-1.12.3.js"></script>

	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

	<script>
	    $.extend( true, $.fn.dataTable.defaults, {
          "ordering": false
         } );

		$(document).on("ready", function(){
			listar();
			guardar();
		});

		$("#btn_listar").on("click", function(){
			listar();
		});

		var guardar = function(){
			$("form").on("submit", function(e){
				e.preventDefault();
				var frm = $(this).serialize();
				$.ajax({
					method: "POST",
					url: "../objetos/cobros.php",
					data: frm
				}).done( function( info ){
				console.log( info );		
					var json_info = JSON.parse( info );
					mostrar_mensaje( json_info );
					limpiar_datos();
					listar();
				});
			});
		}


		var mostrar_mensaje = function( informacion ){
			var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
					texto = "<strong>OPERACION REALIZADA CON EXITO!</strong>";
					color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
					texto = "<strong>Error</strong>, no se ejecutó la consulta.";
					color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
					texto = "<strong>Información!</strong> el usuario ya existe.";
					color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
					texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
					color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
					texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
					color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
					$(this).html("");
					$(this).fadeIn(3000);
			});			
		}

		var limpiar_datos = function(){
			$("#opcion").val("registrar");
			$("#idcredito").val("");
			$("#cliente").val("").focus();
			$("#cantidad").val("");
		}

	    var pos = $("#pos").val();
	    var pos1 = $("#pos1").val();
		var listar = function(){
			$("#cuadro2").slideUp("slow");
				$("#cuadro1").slideDown("slow");
			var table = $("#dt_cliente").DataTable({
				
				"destroy":true,

				"ajax":{
					"method":"POST",
					"url": "clientesinactivos.php",
					"data" :{"pos":pos,"pos1":pos1}
					},
				"columns":[
					{"data":"nombre"},
					{"data":"apellido"},
					{"data":"direccion"},
					{"data":"telefono"},
					{"defaultContent": "<button type='button' class='editar btn btn-warning btn-xs'>Activar cliente</button>"}	
				],
				"language": idioma_espanol
			});

			obtener_data_editar("#dt_cliente tbody", table);
		}

		var obtener_data_editar = function(tbody, table){
			$(tbody).on("click", "button.editar", function(){
				var data = table.row( $(this).parents("tr") ).data();
				var identificacion = $("#identificacion").val( data.identificacion),
						nombre = $("#nombre").val( data.nombre),
						apellido = $("#apellido").val( data.apellido),
						direccion = $("#direccion").val( data.direccion),
						telefono = $("#telefono").val( data.telefono),
						opcion = $("#opcion").val("activar");
                        $("#cuadro2").slideDown("slow");
						$("#cuadro1").slideUp("slow");

			});
		}


		var idioma_espanol = {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Buscar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    }
		}
		
	</script>
</body>
</html>
