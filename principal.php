<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />

<link rel="stylesheet" type="text/css" href="view/css/bootstrap.min.css">
<link href="view/css/estilopagina.css" rel="stylesheet" type="text/css">
<title>Pagina principal</title>
<style type="text/css">
  
  #cargando img{
   margin-left: 600px;
  }
</style>

</head>

<body id="pag">
          
           <header>
              <img src="formularios/logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>
         
           <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="formularios/pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="formularios/cliente.php">Clientes</a>
                    <a class="dropdown-item" href="formularios/empleado.php">Personal</a>
                    <a class="dropdown-item" href="formularios/usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="objetos/clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="formularios/clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="formularios/usuarioindividual.php">Usuarios</a>
                    <a class="dropdown-item" href="formularios/recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="objetos/clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="objetos/clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="formularios/consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="formularios/mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="objetos/revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="objetos/actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="objetos/actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="formularios/ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="formularios/cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Inicio</li>
          </ol>
        </nav>

        <div id="espacio"></div>  
        <div id="cargando" style="display:none;"><img src="cargar1.gif"></div>

        <div class="container">
          <div class="row justify-content-md-center">
            
            <div class="col-lg-6"> 
                     <div class="list-group">
                      <a href="reportes/plantillareporte.php" class="list-group-item list-group-item-action active">REPORTES</a>
                      <a href="formularios/ctipopago.php" class="list-group-item list-group-item-action">Consultar formas de pago</a>
                      <a href="formularios/crutasempleado.php" class="list-group-item list-group-item-action">Consultar rutas</a>
                      <a href="formularios/activarcredito.php" class="list-group-item list-group-item-action">ACTIVAR CRÉDITO</a>
                      <a href="formularios/generararchivoenexcel.php" class="list-group-item list-group-item-action">Generar reporte en excel</a>
                      <a href="objetos/modificarfecha.php" class="list-group-item list-group-item-action" >Modificar fecha festiva</a>
                       <a href="formularios/consultaraccesoasistema.php" class="list-group-item list-group-item-action" >Consultar accesos a sistema</a>
                     </div>
            </div>    
             
          </div>

           <div id="destino"></div>  

           <div id="espacio"></div>  
           
           <div class="row justify-content-md-center">
             <div class="col-lg-6">

                 <footer>
                      
                     <button type="button" id="ejecutar" class="btn btn-outline-danger btn-lg btn-block">Finalizar ingreso de pagos</button>
                  </footer>
               </div>
           </div>
          </div>
    
  <script src="view/js/jquery-3.2.1.min.js"></script>
  <script src="view/js/popper.min.js"></script>
  <script src="view/js/bootstrap.min.js"></script>

    <script language="JavaScript">

    $(document).ready(function(){
            $("#ejecutar").click(function(evento){
            evento.preventDefault();
            if (confirm('ADVERTENCIA ESTA OPERACION ES IRREVOCABLE.. ¿Estas seguro de FINALIZAR?')){
            $("#cargando").css("display", "inline");
            $("#destino").load("objetos/finalizar.php", function(){
            $("#cargando").css("display", "none");

            $("#destino").fadeOut(6000, function(){
                   $(this).html("");
                   $(this).fadeIn();
            });

      });

        }    
   });
})

</script> 

</body>	
</html>     






