<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Registro de sueldo</title>
	<link href="../estilopagina.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">
	<!-- Buttons DataTables -->
	<link rel="stylesheet" href="css/buttons.bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">

</head>
<body>
	    <header>
			<h1>Registro de sueldo</h1>
              
		</header>
		 <nav>
                <ul id="menuHorizontal">
                     <li><a href="../principal.php">Inicio</a>

                     </li>

                     <li>Registros
                         <ul class="submenu">
                             <li><a href="../formularios/pagoporruta.php">Pagos</a></li>
                         </ul>
                     </li>

                     <li>Consultas
                         <ul class="submenu">
                             <li><a href="clientesgeneral45i.php">Clientes</a></li>
                         </ul>    
                            
                     </li>

                     <li>Reportes
                            <ul class="submenu">
                             <li><a href="../reportes/plantillareporte.php">Clientes</a></li>
                         </ul>  
                     </li>
                  
                     <li>Actualizaciones
                          <ul class="submenu">
                             <li><a href="../formularios/mod1Credito3.php">Créditos</a></li>
                         </ul>      
                     </li>

                     <li><a href="../cerrar.php">Salir</a> 
                              
                     </li>
                
                </ul>
           	

           </nav>

	<div class="row" >
		<div id="cuadro2" class="col-sm-12 col-md-12 col-lg-12 ocultar" >
		   <div id="fpago"> 
			<form class="form-horizontal" action="" method="POST">
				<input type="hidden" id="identificacion" name="identificacion" value="0">
				<input type="hidden" id="ruta" name="ruta" value="0">
				<input type="hidden" id="opcion" name="opcion" value="registrar">
				<div class="form-group">
					<label for="fecha1" class="col-sm-3 control-label">Fecha 1</label>
					<div class="col-sm-8"><input id="fecha1" name="fecha1" type="text" class="form-control input-sm" required placeholder="Ej. 01/01/2017"></div>				
				</div>
				<div class="form-group">
					<label for="fehca2" class="col-sm-3 control-label">Fecha 2</label>
					<div class="col-sm-8"><input id="fecha2" name="fecha2" type="text" class="form-control input-sm" required placeholder="Ej. 01/01/2017"></div>
				</div>
				<div class="form-group">
					<label for="quincena" class="col-sm-3 control-label">Quincena</label>
					<div class="col-sm-8"><input id="quincena" name="quincena" type="number" class="form-control input-sm" required  min="1" max="2" placeholder="Solo un digito [ 1  o  2 ]"></div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input id="" type="submit" class="btn btn-success" value="Pagar">
						<input id="btn_listar" type="button" class="btn btn-info" value="Listar">
					</div>
				</div>
			</form>
			</div>
			<div class="col-sm-offset-2 col-sm-8">
				<!--<p class="mensaje"></p> -->
			</div>
			
		</div>
	</div>
	<div class="row">
		<div id="cuadro1" class="col-sm-12 col-md-12 col-lg-12">
			<div class="col-sm-offset-2 col-sm-8">
				<h3 class="text-center"> <small class="mensaje"></small></h3>
			</div>
			<div class="table-responsive col-sm-12">		
				<table id="dt_cliente" class="table table-bordered table-hover table-condensed stripe" cellspacing="0" width="100%">
					<thead bgcolor="#58ACFA">
						<tr>
							  <th >Ruta</th>
                              <th style="width: 250px;">Nombre</th>
                               <th style="width: 250px;">Apellido</th>
                               <th >Cargo</th>
                              <th colspan="2"></th>											
						</tr>
					</thead>					
				</table>
			</div>			
		</div>		
	</div>
	
	<script src="js/jquery-1.12.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap.js"></script>
	<!--botones DataTables-->	
	<script src="js/dataTables.buttons.min.js"></script>
	<script src="js/buttons.bootstrap.min.js"></script>

	<script>
	    $.extend( true, $.fn.dataTable.defaults, {
          "ordering": false
         } );

		$(document).on("ready", function(){
			listar();
			guardar();
		});

		$("#btn_listar").on("click", function(){
			listar();
		});

		var guardar = function(){
			$("form").on("submit", function(e){
				e.preventDefault();
				var frm = $(this).serialize();
				$.ajax({
					method: "POST",
					url: "cobros.php",
					data: frm
				}).done( function( info ){
				console.log( info );		
					var json_info = JSON.parse( info );
					mostrar_mensaje( json_info );
					limpiar_datos();
					listar();
				});
			});
		}


		var mostrar_mensaje = function( informacion ){
			var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
					texto = "<strong>OPERACION REALIZADA CON EXITO!</strong>";
					color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
					texto = "<strong>Error</strong>, no se ejecutó la consulta.";
					color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
					texto = "<strong>Información!</strong> el usuario ya existe.";
					color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
					texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
					color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
					texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
					color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
					$(this).html("");
					$(this).fadeIn(3000);
			});			
		}

		var limpiar_datos = function(){
			$("#opcion").val("registrar");
			$("#idcredito").val("");
			$("#cliente").val("").focus();
			$("#cantidad").val("");
		}

	
		var listar = function(){
			$("#cuadro2").slideUp("slow");
				$("#cuadro1").slideDown("slow");
			var table = $("#dt_cliente").DataTable({
				
				"destroy":true,

				"ajax":{
					"method":"POST",
					"url": "empleado22.php"
					},
				"columns":[
					{"data":"id_ruta"},
					{"data":"nombre"},
					{"data":"apellido"},
					{"data":"cargo"},
					{"defaultContent": "<button type='button' class='editar btn btn-success btn-xs'>Gestionar pago</button>"}	
				],
				"language": idioma_espanol
			});

			obtener_data_editar("#dt_cliente tbody", table);
		}

		var obtener_data_editar = function(tbody, table){
			$(tbody).on("click", "button.editar", function(){
				var data = table.row( $(this).parents("tr") ).data();
				var identificacion = $("#identificacion").val( data.identificacion),
				    ruta= $("#ruta").val( data.id_ruta),
						opcion = $("#opcion").val("pagarsueldo");
                        $("#cuadro2").slideDown("slow");
						$("#cuadro1").slideUp("slow");

			});
		}


		var idioma_espanol = {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Buscar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    }
		}
		
	</script>
</body>
</html>
