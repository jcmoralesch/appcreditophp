<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
	<title>Reporte cobros</title>
	<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../view/cssDT/dataTables.bootstrap.min.css">
	<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../view/cssDT/buttons.bootstrap.min.css">
	<link rel="stylesheet" href="../view/cssDT/font-awesome.min.css">

	
</head>
<body id="pag">
    <header>
			<img src="../formularios/logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"];
              ?>
              
		</header>
		 <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Cobros efectuados</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
           <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../formularios/pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="../formularios/cliente.php">Clientes</a>
                    <a class="dropdown-item" href="../formularios/empleado.php">Personal</a>
                    <a class="dropdown-item" href="../formularios/usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="../formularios/clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="../formularios/usuarioindividual.php">Usuarios</a>
                    <a class="dropdown-item" href="../formularios/recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="../formularios/consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../formularios/mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="../formularios/ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="../formularios/cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>
        <input type="hidden"  id="fecha1" name="fecha1" value="<?php echo $_POST['fecha1'];?>">
        <input type="hidden"  id="fecha2" name="fecha2" value="<?php echo $_POST['fecha2'];?>">

        
	<div class="container">    
	<div class="row">
		<div id="cuadro1" class="col-sm-12 col-md-12 col-lg-12">
			<div class="col-sm-offset-2 col-sm-8">
				<h3 class="text-center"> <small class="mensaje"></small></h3>
			</div>
			<div class="table-responsive">		
				<table id="dt_cliente" class="table table-bordered table-hover table-condensed table-sm table responsive" >
					<thead bgcolor="#58ACFA">
						<tr>
							  <th>Código</th>
                              <th>Nombre</th>
                              <th>Apellido</th>
		                      <th>Cantidad</th>
		                      <th>Mora</th>	
		                      <th>Fecha deposito</th>	
		                      <th>Hora deposito</th>								
						</tr>
					</thead>					
				</table>
			</div>			
		</div>		
	</div>
	</div>
		<script src="../view/js/jquery-3.2.1.min.js"></script>
     <script src="../view/js/popper.min.js"></script>
     
	 <script src="../view/js/jquery-1.12.3.js"></script>
	 <script src="../view/js/bootstrap.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
	 <script src="../view/jsDT/dataTables.buttons.min.js"></script>
	<script src="../view/jsDT/buttons.bootstrap.min.js"></script>
    
	<!--Libreria para exportar Excel-->
	<script src="../view/jsDT/jszip.min.js"></script>
	<!--Librerias para exportar PDF-->
	<script src="../view/jsDT/pdfmake.min.js"></script>
	<script src="../view/jsDT/vfs_fonts.js"></script>
	<!--Librerias para botones de exportación-->
	<script src="../view/jsDT/buttons.html5.min.js"></script>

	<script>
	    $.extend( true, $.fn.dataTable.defaults, {
          "ordering": false
         } );

		$(document).on("ready", function(){

			listar();
			
		});
        
	
        var fecha1 = $("#fecha1").val();
        var fecha2 = $("#fecha2").val();

		var listar = function(){
			var table = $("#dt_cliente").DataTable({
				
				"destroy":true,

				"ajax":{
					"method":"POST",
					"url": "consultarcobros.php",
					"data" :{"fecha1":fecha1,"fecha2":fecha2}
					},
				"columns":[
					{"data":"identificacion"},
				    {"data":"nombre"},
					{"data":"apellido"},
					{"data":"cantidad"},
					{"data":"mora"},
					{"data":"fecha_deposito"},
					{"data":"hora"}
					
				],
				
				"language": idioma_espanol,
				"dom":"<'row'<'form-inline' <'col-sm-offset-10'B>>>"
					 +"<'row' <'form-inline' <'col-sm-1'f>>>"
					 +"<rt>"
					 +"<'row'<'form-inline'"
					 +"<'col-sm-12 col-md-12 col-lg-12'>"
					 +"<'col-sm-12 col-md-12 col-lg-12'p>>>",
				"buttons":[
                     {
		                extend:    'excelHtml5',
		                text:      '<i class="fa fa-file-excel-o"></i>',
		                className: 'btn btn-success',
		                titleAttr: 'Excel'
		            },
		             {
		                extend:    'csvHtml5',
		                text:      '<i class="fa fa-file-text-o"></i>',
		                titleAttr: 'CSV'
		            },
		            {
		                extend:    'pdfHtml5',
		                text:      '<i class="fa fa-file-pdf-o"></i>',
		                titleAttr: 'PDF'
		            }
				]
			});
		}


		var idioma_espanol = {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Buscar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    }
		}
		
	</script>
</body>
</html>
