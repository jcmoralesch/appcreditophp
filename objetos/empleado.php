<?php
       session_start();
          if(!isset($_SESSION["usuario"])){
               header("Location:../index.html");
          }

      require"../clases/empleado.php";

      $identificacion=$_POST['identificacion'];
      $nombre=$_POST['nombre'];
      $apellido=$_POST['apellido'];
      $direccion=$_POST['direccion'];
      $cargo=$_POST['cargo'];
      $ruta=$_POST['ruta'];
      $telefono=$_POST['telefono']; 
      
      $nombre=strtoupper($nombre);
      $apellido=strtoupper($apellido);
      $direccion=strtoupper($direccion);
      $cargo=strtoupper($cargo);

      $empleado = new Empleado();

      $empleado->setIdentificacion($identificacion);
      $empleado->setNombre($nombre);
      $empleado->setApellido($apellido);
      $empleado->setDireccion($direccion);
      $empleado->setCargo($cargo);
      $empleado->setRuta($ruta);
      $empleado->setTelefono($telefono);

      $empleado->registrar();

?>