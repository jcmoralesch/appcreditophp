<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
	<title>Activar credito nuevo</title>

	<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../view/cssDT/dataTables.bootstrap.min.css">
	<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">
	
</head>
<body id="pag">
      <?php
         require_once "../clases/tipocredito.php"; 
         $tCredito = new TipoCredito();
      ?>
	    <header>
	    	<img src="../formularios/logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"]."<br><br>";
              ?> 
		</header>

		  <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Actualizar datos mora</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../formularios/pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="../formularios/cliente.php">Clientes</a>
                    <a class="dropdown-item" href="../formularios/empleado.php">Personal</a>
                    <a class="dropdown-item" href="../formularios/usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="../formularios/clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="../formularios/usuarioindividual.php">Usuarios</a>
                    <a class="dropdown-item" href="../formularios/recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="../formularios/consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../formularios/mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="../formularios/ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="../formularios/cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>

    <div class="container-fluid">    

	<div class="row" >
		<div id="cuadro2" class="col-sm-5 col-md-5 col-lg-5 ocultar" >
		   <div id="margenform" class="border border-info"> 
			<form class="form-horizontal" action="" method="POST">
				<input type="hidden" id="idmora" name="idmora" value="0">
				<input type="hidden" id="opcion" name="opcion" value="registrar">
				<div class="form-group">
					<label for="cliente" class="col-sm-4 control-label">Cliente:</label>
					<div class="col-sm-8"><input id="cliente" name="cliente" type="text" class="form-control input-sm" required readonly></div>				
				</div>
				<div class="form-group">
					<label for="mora" class="col-sm-4 control-label">Mora:</label>
					<div class="col-sm-8"><input id="mora" name="mora" type="text" class="form-control input-sm" required></div>				
				</div>
				<div class="form-group">
					<label for="dmora" class="col-sm-4 control-label">Días en mora:</label>
					<div class="col-sm-8"><input id="dmora" name="dmora" type="text" class="form-control input-sm" required ></div>
				</div>

				<div class="form-group">
					<label for="acumulado" class="col-sm-4 control-label">Total acumulado:</label>
					<div class="col-sm-8"><input id="acumulado" name="acumulado" type="text" required class="form-control input-sm"></div>
				</div>

				<div class="form-group">
					<label for="adelantado" class="col-sm-4 control-label">Días adelantado:</label>
					<div class="col-sm-8"><input id="adelantado" name="adelantado" type="text" class="form-control input-sm" required></div>
				</div>

				<div class="form-group">
					<label for="tadelantado" class="col-sm-4 control-label">Total adelantado:</label>
					<div class="col-sm-8"><input id="tadelantado" name="tadelantado" type="text" class="form-control input-sm" required></div>
				</div>
				

				<div class="form-group">
					<div class="col-sm-offset-1 col-sm-11">
						<input id="" type="submit" class="btn btn-success btn-md btn-block" value="Actualizar datos">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-1 col-sm-11">
						<input id="btn_listar" type="button" class="btn btn-info btn-md btn-block" value="Listar">
					</div>
				</div>
			</form>
			</div>
			<div class="col-sm-offset-2 col-sm-8">
				<!--<p class="mensaje"></p> -->
			</div>
			
		</div>
	</div>
	<div class="row">
		<div id="cuadro1" class="col-sm-12 col-md-12 col-lg-12">
			<div class="col-sm-offset-2 col-sm-8">
				<h3 class="text-center"> <small class="mensaje"></small></h3>
			</div>
			<div class="table-responsive col-sm-12">		
				<table id="dt_cliente" class="table table-bordered table-hover table-condensed stripe" cellspacing="0" width="100%">
					<thead bgcolor="#58ACFA">
						<tr>
						      <th >Id</th>
						      <th >Código</th>
							  <th style="width: 350px;">Cliente</th>
                              <th >Mora g.</th>
							  <th >Dias mora</th>
                              <th>Tot. acumulado</th>
		                      <th>Dias ad.</th>
		                      <th>T. adelantado</th>
		                      <th></th>
                              <th colspan="2"></th>											
						</tr>
					</thead>					
				</table>
			</div>			
		</div>		
	</div>

	</div>

	<script src="../view/js/jquery-3.2.1.min.js"></script>
     <script src="../view/js/popper.min.js"></script>
     <script src="../view/js/bootstrap.min.js"></script>
	 <script src="../view/js/jquery-1.12.3.js"></script>
    
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

	<script>
	    $.extend( true, $.fn.dataTable.defaults, {
          "ordering": false
         } );

		$(document).on("ready", function(){
			listar();
			actualizar();
		});

		$("#btn_listar").on("click", function(){
			listar();
		});

		var actualizar = function(){
			$("form").on("submit", function(e){
				e.preventDefault();
				var frm = $(this).serialize();
				$.ajax({
					method: "POST",
					 encoding:"UTF-8",
					url: "cobros.php",
					data: frm
				}).done( function( info ){
				console.log( info );		
					var json_info = JSON.parse( info );
					mostrar_mensaje( json_info );
					limpiar_datos();
					listar();
				});
			});
		}


		var mostrar_mensaje = function( informacion ){
			var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
					texto = "<strong>OPERACION REALIZADA CON EXITO!</strong>";
					color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
					texto = "<strong>Error</strong>, no se ejecutó la consulta.";
					color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
					texto = "<strong>Información!</strong> el usuario ya existe.";
					color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
					texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
					color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
					texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
					color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
					$(this).html("");
					$(this).fadeIn(3000);
			});			
		}

		var limpiar_datos = function(){
			$("#opcion").val("registrar");
			$("#mora").val("");
			$("#cliente").val("").focus();
			$("#dmora").val("");
			$("#acumulado").val("");
			$("#adelantado").val("");
			$("#dadelantado").val("");
		}

	
		var listar = function(){
			$("#cuadro2").slideUp("slow");
				$("#cuadro1").slideDown("slow");
			var table = $("#dt_cliente").DataTable({
				
				"destroy":true,

				"ajax":{
					"method":"POST",
					"url": "consultarmoraadelantado.php"
					},
				"columns":[
				    {"data":"id_mora"},
				    {"data":"identificacion"},
					{"data":"cliente"},
					{"data":"mora_generada"},
					{"data":"dias_mora"},
					{"data":"total_de_mora"},
					{"data":"adelantado"},
					{"data":"total_adelantado"},
					{"defaultContent": "<button type='button' class='editar btn btn-warning btn-xs'>Actualizar datos</button>"}	
				],
				"language": idioma_espanol
			});

			obtener_data_editar("#dt_cliente tbody", table);
		}

		var obtener_data_editar = function(tbody, table){
			$(tbody).on("click", "button.editar", function(){
				var data = table.row( $(this).parents("tr") ).data();
				var identificacion = $("#idmora").val( data.id_mora),
						cliente = $("#cliente").val( data.cliente),
						mora = $("#mora").val( data.mora_generada),
						dmora = $("#dmora").val( data.dias_mora),
						acumulado = $("#acumulado").val( data.total_de_mora),
						adelantado = $("#adelantado").val( data.adelantado),
						tadelantado = $("#tadelantado").val( data.total_adelantado),
						opcion = $("#opcion").val("actualizarmora");
                        $("#cuadro2").slideDown("slow");
						$("#cuadro1").slideUp("slow");

			});
		}


		var idioma_espanol = {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Buscar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    }
		}
		
	</script>
</body>
</html>
