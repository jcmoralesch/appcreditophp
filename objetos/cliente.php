<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}

      require"../clases/cliente.php";
      $identificacion=$_POST['codigo'];
      $nombre=$_POST['nombre'];
      $apellido=$_POST['apellido'];
      $direccion=$_POST['direccion'];
      $telefono=$_POST['telefono'];
      $ruta=$_POST['ruta'];
      $credito=$_POST['credito1']; 
      $pago=$_POST['pago'];
      $cantidad=$_POST['cantidad'];

      $cliente = new Cliente();

      $cliente->setIdentificacion($identificacion);
      $verificar=$cliente->consultar_usuario();

      if($pago=="DIARIO_25"){
        $pago="1";
      }
      else if($pago=="SEMANAL_8"){
         $pago="2";
      }

      if(empty($identificacion) || empty($nombre) || empty($apellido) || empty($direccion) || empty($telefono) || empty($cantidad)){
               echo '<div class="alert alert-danger" role="alert">!!!ERROR!!! Operacion rechazada.. Porfavor rellene todos los campos </div>';

      }
      elseif($verificar==false){
               print('<div class="alert alert-danger" role="alert">!!ERROR!!! EXISTE  UN USUARIO CON CODIGO SIMILAR..</div>');
      } 

      else{

           $identificacion=strtoupper($identificacion);
           $nombre=strtoupper($nombre);
           $apellido=strtoupper($apellido);
           $direccion=strtoupper($direccion);

          $cliente->setNombre($nombre);
          $cliente->setApellido($apellido);
          $cliente->setDireccion($direccion);
          $cliente->setTelefono($telefono);
          $cliente->setRuta($ruta);
          $cliente->setCredito($credito);     
          $cliente->setPago($pago);
          $cliente->setCantidad($cantidad);

          $validar=$cliente->registrar();

          if($validar){
              echo '<div class="alert alert-success" role="alert">!!!OK!!! CLIENTE REGISTRADO CORRECTAMENTE </div>';
          }
          else{
          echo'<div class="alert alert-success" role="alert">!!!Error !!! No se pudo registrar/div>';
         }
    }

?>
