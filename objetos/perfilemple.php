<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../estilopagina.css" rel="stylesheet" type="text/css">
<link href="../bootstrap.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<title>Perfil cliente</title>
</head>

<body>
     <?php
         require_once "../clases/empleado.php"; 
         $empleado = new Empleado();

         $identificacion=$_POST['id'];
      ?>
    <div id="contenedor"> 

           <header>
              <h1>Perfil empleado</h1>
              <img src="logo.gif">
              <?php
                echo "Usuario: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>           
  <br>
  <div class="list-group">
  <a href="../formularios/sueldos12pago.php" class="list-group-item active">
    Volver a perfiles de usuarios
  </a>
</div>
           <section id="clientes">  
                
                       <?php 
                                  $array_emple=$empleado->perfilemple($identificacion); 

                                 foreach($array_emple as $elemento){
                                      $identificacion= $elemento['identificacion'];
                                      $nombre= $elemento['nombre'];
                                      $apellido= $elemento['apellido'];
                                      $direccion= $elemento['direccion'];
                                      $cargo= $elemento['cargo'];
                                      $fecha=$elemento['fecha'];
                                      $idruta= $elemento['id_ruta'];
                                      $telefono=$elemento['telefono'];
                                      $usuario= $elemento['usuario'];
                                      $privilegio= $elemento['privilegio'];
                                      $sueldobase= $elemento['sueldo_base'];
                                      $depreciacion= $elemento['depreciacion_vehiculo'];
                                      $combustible=$elemento['combustible'];
                                      $saldotelefono= $elemento['saldo_telefono'];        
                                      $porcentajerecaudo=$elemento['porcentaje_recaudo'];
                                      $tiposueldo=$elemento['tipo_sueldo'];
                                 }
                           ?>

                    <div id="datos"> 
                        <h3><label class="control-label">Datos empleado</label></h3>
                        <div><label class="control-label">Identificacion:</label><?php echo $identificacion;?></div>
                        <div><label class="control-label">Nombre: </label><?php echo $nombre;?></div>
                        <div><label class="control-label">Apellido:</label> <?php echo $apellido;?></div>
                        <div><label class="control-label">Dirección: </label><?php echo $direccion;?></div>
                        <div><label class="control-label">Cargo: </label><?php echo $cargo;?></div>
                        <div><label class="control-label">Telefono:</label> <?php echo $telefono;?></div>
                        <div><label class="control-label">Ruta: </label><?php echo $idruta;?></div>
                        <div><label class="control-label">Fecha registro:</label> <?php echo $fecha;?></div>
                    </div>
                     
                    <div id="credito">
                    <h3 align="center">Datos complementarios</h3>
                        <div class="r"><label class="control-label">Usuario:</label> <?php echo $usuario;?></div>
                        <div class="r"><label class="control-label">Privilegio:</label> <?php echo $privilegio;?></div>
                        <div class="r"><label class="control-label">sueldo base:</label> <?php echo $sueldobase;?></div>
                        <div class="r"><label class="control-label">Depreciación de vehículo:</label> <?php echo $depreciacion;?></div> 
                        <div class="r"><label class="control-label">Combustible: </label><?php echo $combustible;?></div>
                        <div class="r"><label class="control-label">Saldo teléfono:</label> <?php echo $saldotelefono;?></div>
                        <div class="r"><label class="control-label">Porcentaje en recaudo:</label> <?php echo $porcentajerecaudo;?></div>
                        <div class="r"><label class="control-label">Tipo sueldo:</label><?php echo $tiposueldo;?></div>
                    </div> 
                
           </section>
           <br>
           <br>
           <footer>
           	  <a href="../formularios/sueldos12pago.php" class="list-group-item active">
    Volver a perfiles de usuarios
  </a>
           </footer>
    </div>       
</body>	
</html>