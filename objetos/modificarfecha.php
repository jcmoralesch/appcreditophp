<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
	<title>Modificar fecha</title>
  <link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">
	<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<style type="text/css">

  #cargando img{
   margin-left: 600px;
  }
</style>


</head>
<body id="pag">
	    <header>
			<img src="../formularios/logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"];
              ?>

		</header>

     <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Modificar fecha festiva</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../formularios/pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="../formularios/cliente.php">Clientes</a>
                    <a class="dropdown-item" href="../formularios/empleado.php">Personal</a>
                    <a class="dropdown-item" href="../formularios/usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="../formularios/clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="../formularios/usuarioindividual.php">Usuarios</a>
                    <a class="dropdown-item" href="../formularios/recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="../formularios/consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../formularios/mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="../formularios/ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="../formularios/cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Inicio</li>
          </ol>
        </nav>

            <div id="cargando" style="display:none;"><img src="../cargar1.gif"></div>


            <div id="espacio2"></div>

           <div class="container">
               <div class="row justify-content-md-center">
                 <div class="col-lg-5">
                <form role="form" action="#" method="post" id="operar" name="operar">
                 <div class="card">
                  <h5 class="card-header">.</h5>
                  <div class="card-body">

                    <p class="card-text"><div class="form-group">
                              <label>Ingrese fecha a modificar:</label>
                              <input type="text" name="fecha" id="fecham"  class="form-control date">
                       </div></p>
                     <div id="boton" class="form-group">

                                <a id="ejecutar" class="btn btn-outline-danger btn-lg btn-block"> Modificar</a>
                      </div>
                  </div>
                </div>
                </form>
                </div>
              </div>
          </div>


           <div id="mostrarmensaje"></div>

	    <script src="../view/js/jquery-3.2.1.min.js"></script>
     <script src="../view/js/popper.min.js"></script>
     <script src="../view/js/bootstrap.min.js"></script>
		 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

     <script language="JavaScript">

    $(document).ready(function(){

			       $(".date").datepicker({dateFormat: 'yy/mm/dd'});
            $("#ejecutar").click(function(evento){
            evento.preventDefault();
            var fecha=$("#fecham").val();
            if (confirm('ADVERTENCIA ESTA OPERACION ES IRREVOCABLE.. ¿Estas seguro de MODIFICAR?')){
            $("#cargando").css("display", "inline");
            $("#mostrarmensaje").load("modificarfechafestivo.php",{fecha:fecha}, function(){
            $("#cargando").css("display", "none");

            $("#mostrarmensaje").fadeOut(6000, function(){
                   $(this).html("");
                   $(this).fadeIn();
            });

      });

        }
   });
})

</script>

</body>
</html>
