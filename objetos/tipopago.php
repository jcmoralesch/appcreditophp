<?php

      session_start();
          if(!isset($_SESSION["usuario"])){
               header("Location:../index.html");
          }
          
      require"../clases/tipopago.php";

      $tipo=$_POST['tipo'];
      $pagos=$_POST['pagos'];
      $interes=$_POST['interes'];
      $plazo=$_POST['plazo'];
      
      $tipo=strtoupper($tipo);
      $plazo=strtoupper($plazo);

      $pago = new Pago();

      $pago->setTipoPago($tipo);
      $pago->setPago($pagos);
      $pago->setInteres($interes);
      $pago->setPlazo($plazo);

      $pago->registrar();

?>