<?php
      session_start();//aqui se reanuda la sesion..
          if(!isset($_SESSION["usuario"])){
               header("Location:../index.html");
          }
          
      require"../clases/usuario.php";

      $usuario=$_POST['usuario'];
      $password=$_POST['pass'];
      $privilegio=$_POST['privilegio'];
      $id_empleado=$_POST['id_emple'];
      
      $privilegio=strtoupper($privilegio);

      $pass_cifrado=password_hash($password,PASSWORD_DEFAULT,array("cost"=>12));

      $usua = new Usuario();

      $usua->setUsuario($usuario);
      $usua->setPassword($pass_cifrado);
      $usua->setPrivilegio($privilegio);
      $usua->setIdEmpleado($id_empleado);
      

      $usua->registrar();

?>