<?php session_start(); if(!isset($_SESSION["usuario"])){ header("Location:../index.html");} ?>
<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
   <title>Enviar mensaje</title>
   <link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">
</head>
<body>
<?php          
      require"../clases/tipocredito.php";

       $tcredito = new TipoCredito();

       $fecha=$_POST['fecha'];

       $tcredito->setFecha($fecha);
      
      $confirmar=$tcredito->modificarFecha();
      if($confirmar){

      	echo '<div class="alert alert-success" role="alert"> <span></span>   !!!OK!!! OPERACION REALIZADA CON EXITO</div>';
      }
      else{
      	  echo '<div class="alert alert-danger" role="alert"> <span></span>   !!!Error !!! Algo salio mal informe de inmediato a departamento IT</div>';
      }

?>
 <script src="js/bootstrap.min.js"></script>
</body>
</html>