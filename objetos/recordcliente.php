<?php 
    require_once"../clases/recordocliente.php";

    $record= new RecordCliente();

    $id=$_POST['cod'];

    $record->setIdCliente($id);

    $array_record=$record->consultar_record();
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>

<body>
   
   
       <div id="chart_div"></div>

       <br/>
    <div id="btn-group">
      
    </div>

<script type="text/javascript">
	google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Fecha', 'Atrasos', 'Pagos puntuales'],
          <?php 
                 $contador=0;
               foreach ($array_record as $elemento) { ?>
          ['<?php echo $elemento['fecha'];?>',<?php echo $elemento['atrasos'];?>, <?php echo $elemento['pagos_puntuales']; $contador++?>],      

          <?php    }?>
        ]);

        var options = {
          chart: {
            title: 'CLIENTE: <?php echo $elemento['nombre'] ; echo  $elemento['apellido']; ?>',
            subtitle: 'Record de pagos de  <?php echo $contador ?> creditos',
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#d95f02', '#1b9e77', '#7570b3']
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));

        var btns = document.getElementById('btn-group');

        btns.onclick = function (e) {

          if (e.target.tagName === 'BUTTON') {
            options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
            chart.draw(data, google.charts.Bar.convertOptions(options));
          }
        }
      }
</script>
</body>
</html>