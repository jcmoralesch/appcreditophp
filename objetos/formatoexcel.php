<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}
    include("../conexion.php");

	$query = "select `C`.`identificacion` AS `identificacion`,`C`.`id_ruta` AS `id_ruta`,concat(`C`.`nombre`,' ',`C`.`apellido`) AS `cliente`,`C`.`nombre` AS `nombre`,`C`.`apellido` AS `apellido`,`C`.`telefono` AS `telefono`,`R`.`id_credito` AS `id_credito`,`R`.`monto` AS `monto`,concat((`R`.`monto_total` - `R`.`monto`)) AS `interes`,concat((`E`.`pagos_realizados` + `E`.`pagos`)) AS `cuotas`,`R`.`cuota` AS `cuota`,date_format(`R`.`fecha_registro`,'%d/%m/%Y') AS `fecha`,date_format(`R`.`fecha_vencimiento`,'%d/%m/%Y') AS `vencimiento`,`L`.`no_ciclo` AS `no_ciclo`,`E`.`total_pagado` AS `total_pagado`,`E`.`saldo` AS `saldo`,`E`.`pagos_realizados` AS `pagos_realizados`,`E`.`pagos` AS `pagos`,`M`.`mora_generada` AS `mora_generada`,`M`.`dias_mora` AS `dias_mora`,`M`.`total_de_mora` AS `total_de_mora`,`R`.`id_tipo_credito` AS `id_tipo_credito`,`E`.`estado_pago` AS `estado_pago`,`M`.`adelantado` AS `adelantado` from ((((`cliente` `C` join `credito` `R`) join `ciclo` `L`) join `estado_cuenta` `E`) join `mora` `M`) where ((`C`.`identificacion` = `L`.`identificacion`) and (`C`.`identificacion` = `R`.`identificacion`) and (`R`.`id_credito` = `M`.`id_credito`) and (`R`.`id_credito` = `E`.`id_credito`) and (`R`.`status` = 'P') and (`E`.`status` = 'P')) order by `C`.`id_ruta`";
	$resultado = mysqli_query($conexion, $query);


	if (!$resultado){
		die("Error");
	}
	else{
		while($data=mysqli_fetch_assoc($resultado)){
			$arreglo["data"][]=array_map("utf8_encode",$data); 
		}

		echo json_encode($arreglo);
	}

	mysqli_free_result($resultado);
	mysqli_close($conexion);
	
?>