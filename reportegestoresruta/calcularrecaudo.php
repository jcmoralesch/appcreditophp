<?php
 session_start();
 if(!isset($_SESSION["usuario"])){
      header("Location:../index.html");
  }

include 'plantilla.php';
include '../clases/empleado.php';

$fecha1=$_POST['fecha1'];
$fecha2=$_POST['fecha2'];
$ruta=$_POST['ruta'];

$recaudo= new Empleado();

$array_recaudo=$recaudo->calcularRecaudo($fecha1,$fecha2,$ruta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');

$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(30,6,'Cantidad cobrada',1,0,'C',1);
$pdf->Cell(50,6,'Fecha de cobro',1,1,'C',1);


$total=0;


foreach ($array_recaudo as $elemento) {
	$pdf->Cell(30,6,$elemento['cant'],1,0,'C');
	$pdf->Cell(50,6,$elemento['fech'],1,1,'C');

	$total=$total+$elemento['cant'];
}
$porcent=$total*0.025;
$sum=$porcent+$total;
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,7,'Total cobrado: '.$total,0,0,'C');

$pdf->Cell(30,7,'Porcentaje: '.$porcent,0,1,'C');
$pdf->Output();

?>