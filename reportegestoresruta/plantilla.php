<?php
   require '../fpdf/fpdf.php';

   class PDF extends FPDF{

   	function Header(){
   	//	$this->Image('../formularios/logo.gif',5,5,50);
   		$this->SetFont('Arial','B',12);
   		$this->Cell(30);
   		$this->Cell(90,10,'Reporte de ruta',0,0,'C');
         $this->SetFont('Arial','B',8);
         $this->Cell(10,10,'Fecha '.date('d-m-Y'),0,0,'C');
   		$this->Ln(15);
   	}

   	function Footer(){
   		$this->SetY(-15);
   		$this->SetFont('Arial','I',8);
   		$this->Cell(0,10, 'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
   	}
   }
?>
