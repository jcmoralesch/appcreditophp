<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../estilopagina.css" rel="stylesheet" type="text/css">
<title>Reportes</title>
</head>

<body>

     <?php
          require_once "../clases/tipocredito.php"; 
          $tCredito = new TipoCredito();
      ?>
     <div id="contenedor">
           <header>
              <h1>Area de reportes</h1>
               <h3>CREDICONFINS</h3>
              <?php
                echo "Buen dia: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>

           <nav>
                <ul id="menuHorizontal">
                    <li><a href="../principal.php">Inicio</a>

                     </li>


                     <li>Reportes
                           <ul class="submenu">
                             <li><a href="reportegestor123e.php">Reporte de ruta</a></li>
                         </ul>   
                     </li>

                     <li><a href="../cerrar.php">Salir</a> 
                              
                     </li>
                
                </ul>
           </nav>
           <br><br><br>
           <form action="reporteGestor.php" method="post" target="_blank">
                    <label>Reporte general de clientes:</label>
      	            <input type="submit" name="registrar" value="Generar">

           </form>
           <br><br>
           <form action="porruta.php" method="post" target="_blank">
                    <label>Reporte por ruta:</label>
                    <select name="ruta" required>
                                             <?php
                                                  $array_ruta=$tCredito->consultarRutaGestor();

                                                foreach($array_ruta as $elemento){
                                                   echo"<option>";
                                                   echo $elemento['id_ruta'];
                                                   echo"</option>";
                                                  }
                                               ?>
                                        </select>
                    <input type="submit" name="registrar" value="Generar">

           </form>
           <br><br>
           <form action="calculopagos.php" method="post" target="_blank">
                    <label>Calcular pagos:</label>
                    <input type="text" name="cantidad" require placeholder="ingrese cantidad Ej.(1000.00)">
                    <input type="submit" name="registrar" value="Generar">

           </form>

           <br><br>
           <form action="calendario.php" method="post" target="_blank">
                    <label>Calendario de pagos:</label>
                    <input type="text" name="identificacion" require placeholder="ingrese identificacion del cliente">
                    <input type="submit" name="registrar" value="Generar">

           </form>

           <footer>
           	
           </footer>
     <div id="contenedor">      
</body>	
</html>