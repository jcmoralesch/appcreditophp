<?php
    require "configuracion.php";

    class Conexion{

        protected $conexionDB;

        public function Conexion(){
            try{
                $this->conexionDB= new PDO('mysql:host=localhost;dbname=controlcreditos;charset=utf8',DB_USUARIO,DB_PASSWORD);
                $this->conexionDB->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $this->conexionDB->exec(DB_CHARSET);

                return $this->conexionDB;
            }
            catch(Exception $e){
            	echo "Error. No se puede conectar a la base de datos";
                echo"La linea de error es: ".$e->getLine();

            }
        }
    }
?>
