$(document).ready(function(){
    $('#galeria a').fancybox({
         overlayColor:"#797e79",
         overlayOpacity:.6,
         transitionIn:"elastic",
         transitionOut:"elastic",
         titlePosition: "Outside",
         cyclic:true,
    });
});