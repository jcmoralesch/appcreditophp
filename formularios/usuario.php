<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css"> 
<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">

<title>Registro varios</title>
</head>

<body id="pag">

           <header>
           	<img src="logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"];
              ?>
           </header>

           <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Registros varios</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
           <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="cliente.php">Clientes</a>
                    <a class="dropdown-item" href="empleado.php">Personal</a>
                    <a class="dropdown-item" href="usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../objetos/clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="usuarioindividual.php">Usuarios</a>
                     <a class="dropdown-item" href="recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="../objetos/clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="../objetos/revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="../objetos/actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../principal.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Varios</li>
          </ol>
        </nav>
         <div id="espacio"></div>
         <div class="container">
           <div class="row">
               <div class="col-lg-4">
                   <div class="card">
                      <h5 class="card-header text-center">Tipos de pago</h5>
                      <div class="card-body">

                               <form action="../objetos/tipopago.php" method="post">
                  <div class="form-group row">
                        <label for="tipo" class="col-sm-5 col-form-label">Tipo de crédito:</label>
                        <div class="col-sm-7">
                              <input type="text" name="tipo" id="tipo" required autofocus="autofocus" class="form-control form-control-sm">
                        </div>
                       
                  </div>


                  <div class="form-group row">
                        <label for="pagos" class="col-sm-5 col-form-label">No. de pagos:</label>
                        <div class="col-sm-7">
                           <input type="text" name="pagos" id="pagos" required class="form-control form-control-sm">
                        </div>
                  </div>
                  

                  <div class="form-group row">
                       <label for="interes" class="col-sm-5 col-form-label">Interés:</label>
                       <div class="col-sm-7">
                                 <input type="text" name="interes" id="interes" required class="form-control form-control-sm">
                       </div>
                  
                  </div>

                  <div class="form-group row">
                       <label for="plazo" class="col-sm-5 col-form-label">Plazo:</label>
                       <div class="col-sm-7">
                           <input type="text" name="plazo" id="plazo" required class="form-control form-control-sm">
                       </div>
                       
                  </div>

                  <div class="form-group">
                     <input type="submit" name="registrar" value="Registrar" class="btn btn-outline-success btn-md btn-block">
                  </div>
                  <div class="form-group">
                      <input type="reset" name="registrar" value="Cancelar" class="btn btn-outline-danger btn-md btn-block">
                  </div>       

           </form>

                      </div>
                   </div>
               </div>

               <div class="col-lg-4">
                    <div class="card">
                      <h5 class="card-header text-center">Registrar rutas</h5>
                      <div class="card-body">

                            <form role="form" action="../objetos/ruta.php" method="post">
                <div class="form-group row">
                     <label for="id_ruta" class="col-sm-5 col-form-label">Id. ruta:</label>
                     <div class="col-sm-7">
                         <input type="text" name="id_ruta" id="id_ruta" required class="form-control form-control-sm">
                     </div>
                     
                  
                </div>

                <div class="form-group row">
                     
                    <label for="localizacion" class="col-sm-5 col-form-label"> Localizacion:</label>
                    <div class="col-sm-7">
                        <input type="text" name="localizacion" id="localizacion" required class="form-control form-control-sm">
                    </div>
                    
                </div>
                  
                 <div class="form-group">
                     <input type="submit" name="registrar" value="Registrar" class="btn btn-outline-success btn-md btn-block">
                 </div>
                  
           </form>

                      </div>
                   </div>
               </div>
                    
           </div>
         </div>  
           
          

           <footer>
           	
           </footer>
     
     <script src="../view/js/jquery-3.2.1.min.js"></script>
     <script src="../view/js/popper.min.js"></script>
     <script src="../view/js/bootstrap.min.js"></script>          	
</body>
</html>

