<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="../estilopagina.css" rel="stylesheet" type="text/css">
<link href="../Bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="../Bootstrap/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<title>Usuarios de sistema</title>
<style type="text/css">

input{
border: none;
}
</style> 
</head>

<body>
     <?php
         require_once "../clases/tipocredito.php"; 
         $tcredito = new TipoCredito();
      ?>
    <div id="contenedor"> 

           <header>
              <h1>Sueldos</h1>
              <img src="logo.gif">
              <?php
                echo "Usuario: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>

           <nav>
                <ul id="menuHorizontal">
                     <li><a href="../principal.php">Inicio</a>

                     </li>

                     <li>Registros
                         <ul class="submenu">
                             <li><a href="cliente.php">Clientes</a></li>
                             <li><a href="empleado.php">Empleados</a></li>
                             <li><a href="usuario.php">Varios</a></li>

                         </ul>
                     </li>

                     <li>Consultas
                         <ul class="submenu">
                             <li><a href="../objetos/clientesgeneral45i.php">Clientes</a></li>
                             <li><a href="clienteindividual.php">Perfiles</a></li>
                             <li><a href="usuarioindividual.php">Usuarios</a></li>
                         </ul>    
                            
                     </li>

                     <li>Reportes
                            <ul class="submenu">
                             <li><a href="../reportes/plantillareporte.php">Clientes</a></li>
                         </ul>  
                     </li>
                  
                     <li>Actualizaciones
                          <ul class="submenu">
                             <li><a href="mod1Credito3.php">Créditos</a></li>
                         </ul>      
                     </li>

                     <li><a href="../cerrar.php">Salir</a> 
                              
                     </li>
                
                </ul>
           	

           </nav>
           <section id="clientes">  
           
                <table class="table table-bordered table-hover table-condensed">
                    <thead bgcolor="#58ACFA">
                      <th>Id. sueldo</th>
                      <th>Sueldo base</th>
                      <th>Depreciación de vehículo</th>
                      <th>Combustible</th>
                      <th>Saldo teléfono</th>
                      <th>Porcentaje en recaudo</th>
                      <th>Tipo de sueldo</th>
                    </thead>

                       <?php 
                                  $array_credito=$tcredito->consultarSueldo(); 

                                 foreach($array_credito as $elemento){
                                      echo "<tr><td>".$elemento['id_sueldo']."</td><td>";
                                      echo $elemento['sueldo_base']."</td><td>";
                                      echo $elemento['depreciacion_vehiculo']."</td><td>";
                                      echo $elemento['combustible']."</td><td>";
                                      echo $elemento['saldo_telefono']."</td><td>";
                                      echo $elemento['porcentaje_recaudo']."</td><td>";
                                      echo $elemento['tipo_sueldo']."</td></tr>";

                                 }
                          ?>
                          

                 </table>
                
           </section>
          
           <footer>
           	
           </footer>
    </div> 

</body>	
</html>