<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />

<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../view/cssDT/dataTables.bootstrap.min.css">
<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <!-- Buttons DataTables -->
  <title>Record cliente</title>
</head>

<body id="pag">

           <header>
             <img src="logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"];
              ?>
           </header>


           <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Record cliente</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="cliente.php">Clientes</a>
                    <a class="dropdown-item" href="empleado.php">Personal</a>
                    <a class="dropdown-item" href="usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../objetos/clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="usuarioindividual.php">Usuarios</a>
                     <a class="dropdown-item" href="recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="../objetos/clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="../objetos/revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="../objetos/actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>

  <div class="container-fluid">
      <div class="row">
    <div id="cuadro1" class="col-sm-12 col-md-5 col-lg-5">
     
      <div class="table-responsive">    
        <table id="dt_cliente" class="table table-hover table-condensed  table-sm" cellspacing="0" width="100%">
          <thead bgcolor="#58ACFA">
            <tr>
                <th >Código</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>VER</th> 
                <th></th> 
                  
            </tr>
          </thead>          
        </table>
      </div>      
    </div> 

    <div class="col-sm-12 col-lg-7">
       <div id="perfilcliente"></div>
    </div>   
  </div>
  </div>            
  
  
  <script src="../view/js/jquery-3.2.1.min.js"></script>
   <script src="../view/js/popper.min.js"></script>
     <script src="../view/js/bootstrap.min.js"></script>
   <script src="../view/js/jquery-1.12.3.js"></script>
    
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

  <script>
      $.extend( true, $.fn.dataTable.defaults, {
          "ordering": false,
          "info":     false,
          "paging":false,
          "lengthMenu": [[5, 5, 5, -1], [5, 5, 5]]
         } );

    $(document).on("ready", function(){
      listar();
      
    });



    var listar = function(){
      var table = $("#dt_cliente").DataTable({
        
        "destroy":true,

        "ajax":{
          "method":"POST",
          "url": "../objetos/ficheroActualizarCliente.php"
          },
        "columns":[
          {"data":"identificacion"},
          {"data":"nombre"},
          {"data":"apellido"},
          {"defaultContent": "<button type='button' class='editar btn btn-warning btn-xs'>RECORD</button>"}  
        ],
        "language": idioma_espanol
      });

      obtener_data_editar("#dt_cliente tbody", table);
      table.search( 'Ingrese nombre de cliente' ).draw();

      
    }
    
    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
      
         $.ajax({
            url:"../objetos/recordcliente.php",
            data:{cod :data.identificacion},
            type : 'POST',
            dataType :'html',
            success:function(resultado) {
                    $('#perfilcliente').html(resultado);
            },error : function(xhr, status) {
                if (xhr.status == 404) {

                   alert('Requested page not found [404]');

                  }
            }
               });
      });
    }


   
    var idioma_espanol = {
        "sZeroRecords":    "No se encontraron resultados",
        "sSearch":         "Buscar:"
    }
    
  </script>
           
</body>	
</html>