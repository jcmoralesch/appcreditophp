<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />

	<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">  
	<link rel="stylesheet" type="text/css" href="../view/cssDT/dataTables.bootstrap.min.css">
    <link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">
   
	<title>Registrar ubicación geográfica </title>
    
<style type="text/css">
       	
      #map {
        height: 600px;
        
       }

 </style>

</head>
<body id="pag">
	 <header>
          <img src="logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"];
              ?>
     </header>

     <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Ubicación geográfica Uspantan</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="cliente.php">Clientes</a>
                    <a class="dropdown-item" href="empleado.php">Personal</a>
                    <a class="dropdown-item" href="usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../objetos/clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="usuarioindividual.php">Usuarios</a>
                     <a class="dropdown-item" href="recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="../objetos/clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="../objetos/revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="../objetos/actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>
        
        <div class="continer-fluid">
        	
        	<div class="row">
        		<div class="col-lg-8">
        			
        			<div id="map"></div>

        		</div>
        		<div class="col-lg-4">

        			
                       <div id="accordion" role="tablist">
							  <div class="card">
							    <div class="card-header" role="tab" id="headingOne">
							      <h5 class="mb-0">
							        <a data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne">
							          Registrar ubicación geográfica
							        </a>
							      </h5>
							    </div>

							    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
							      <div class="card-body">
                    <div id="espacio"></div>
                                      
        			<div class="row" >
		<div id="cuadro2" class="col-sm-12 col-md-12 col-lg-12 ocultar" >
		   <div id="margenform" class="border border-info"> 
			<form class="form-horizontal" action="" method="POST" id="formubicacion">
				<input type="hidden" id="identificacion" name="identificacion" value="0">
				<input type="hidden" id="opcion" name="opcion" value="registrar">
				<div class="form-group">
					<label for="codigo" class="col-sm-4 control-label">Codigo:</label>
					<div class="col-sm-8"><input id="codigo" name="codigo" type="text" class="form-control input-sm" required></div>				
				</div>
				<div class="form-group">
					<label for="nombre" class="col-sm-4 control-label">Nombre</label>
					<div class="col-sm-8"><input id="nombre" name="nombre" type="text" class="form-control input-sm" required></div>				
				</div>
				<div class="form-group">
					<label for="apellido" class="col-sm-4 control-label">Apellido</label>
					<div class="col-sm-8"><input id="apellido" name="apellido" type="text" class="form-control input-sm" required ></div>
				</div>

				<div class="form-group">
					<label for="cx" class="col-sm-4 control-label">Coordenada X</label>
					<div class="col-sm-8"><input id="cx" name="cx" type="text" class="form-control input-sm" readonly></div>
				</div>

				<div class="form-group">
					<label for="cy" class="col-sm-4 control-label">Coordenada Y</label>
					<div class="col-sm-8"><input id="cy" name="cy" type="text" class="form-control input-sm" readonly></div>
				</div>
				

				<div class="form-group">
					<div class="col-sm-offset-1 col-sm-11">
						<input id="" type="submit" class="btn btn-success btn-md btn-block" value="REGISTRAR">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-1 col-sm-11">
						<input id="btn_listar" type="button" class="btn btn-info btn-md btn-block" value="Listar">
					</div>
				</div>
			</form>
			</div>
			<div class="col-sm-offset-2 col-sm-8">
				<!--<p class="mensaje"></p> -->
			</div>
			
		</div>
	</div>

                          <div class="col-sm-offset-2 col-sm-8">
				<h5 class="text-center"> <small class="mensaje"></small></h5>
			</div>
							        <div class="table-responsive" id="cuadro1">    
							        <table id="dt_cliente" class="table table-hover table-condensed  table-sm" cellspacing="0" width="100%">
							          <thead bgcolor="#58ACFA">
							            <tr>
							                <th>Nombre</th>
							                <th>Apellido</th>
							                <th>Registrar</th> 
							            </tr>
							          </thead>          
							        </table>
							      </div> 
							      </div>
							    </div>
							  </div>
							  <div class="card">
							    <div class="card-header" role="tab" id="headingTwo">
							      <h5 class="mb-0">
							        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="false" aria-controls="collapseTwo">
							          CONSULTAR POR RUTAS
							        </a>
							      </h5>
							    </div>
							    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
							      <div class="card-body">

							      	<button type="button" id="r1" class="btn btn-secondary">RUTA 1</button>
							      	<button type="button" id="r2" class="btn btn-primary">RUTA 2</button>
							      	<button type="button" id="r3" class="btn btn-warning">RUTA 3</button>
							      	<button type="button" id="r4" class="btn btn-info">RUTA 4</button>
							      	<button type="button" id="r5" class="btn btn-primary">TODO</button>
							        
							      </div>
							    </div>
							  </div>
							 
                        </div>


        		</div>

        	</div>
        </div>  
    
    

    <script src="../view/js/jquery-3.2.1.min.js"></script>     <script src="../view/js/popper.min.js"></script>
     <script src="../view/js/bootstrap.min.js"></script> 
     <script src="../view/js/jquery-1.12.3.js"></script>
    
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>


  <script>

  	var marcadores_nuevos=[];

     var customLabel = {
        R1DU: {
          label: 'R1'
        },
        R2DU: {
          label: 'R2'
        },
        R3DU:{
          label: 'R3'
        },
        R4DU:{
        	label: 'R4'
        }
      };

      
       var map;
       
       var posini={lat:15.349018,lng:-90.870659};

       

      function quitar_marcadores(lista){
        	//Recorrer el array de marcadores

        	for(i in lista){
        		//quitar maracador del mapa
        		lista[i].setMap(null);
        	}
        }
    	var marcador;



    	function CenterControl(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click para recenter el mapa';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = 'Volver a centrar';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
          map.setCenter(posini);
        });

      }


        function initMap() {

	         var formubicacion=$("#formubicacion");	

	 
             //var posini1=new google.maps.LatLng(15.349018,-90.870659);
	          map = new google.maps.Map(document.getElementById('map'), {
	          center: posini,
	          zoom: 16,
	          mapTypeId: 'satellite'

             });
            
            var pos="(15.349018,-90.870659)";
		    var marker = new google.maps.Marker({
		      	
	          position:posini,
	          map: map,
	          title:"CREDICONFINS"+pos,
	          visible:true
	        });


	        google.maps.event.addListener(map,"click",function(event){
        	
        	var coordenadas=event.latLng.toString();

        	coordenadas=coordenadas.replace("(","");
        	coordenadas=coordenadas.replace(")","");

        	var lista=coordenadas.split(",");

        	var direccion= new google.maps.LatLng(lista[0],lista[1]);

        	 marcador = new google.maps.Marker({
        		position: direccion, //La posicion del nuevo marcador
        		map:map,// en que mapa se ubicara el marcador
        		animation:google.maps.Animation.DROP,// como aparecera el marcador
        		draggable:false//no permitir el arrastre del marcador
        	});

        	 //Pasar las coordenadas al formulario
        	 formubicacion.find("input[name='cx']").val(lista[0]);
        	 formubicacion.find("input[name='cy']").val(lista[1]);

        	 //Dejar solo 1 marcador en el mapa
        	 //Guradar el marcador en el array

        	 marcadores_nuevos.push(marcador);

        	 google.maps.event.addListener(marcador,"click",function(){
        	 	
        	 });

        	//Antes de ubicar el marcador en el mapa, quitar todos los demas y asi solo dejar 1
        	quitar_marcadores(marcadores_nuevos);
        	marcador.setMap(map);
        });
        

          var infoWindow = new google.maps.InfoWindow;

          downloadUrl('../objetos/objetosxml.php', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var name = markerElem.getAttribute('name');
              var ape=markerElem.getAttribute('apellido');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name+' '+ape
              infowincontent.appendChild(strong);
               infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              infowincontent.appendChild(document.createElement('br'));
              
              var dir=document.createElement('text');
              dir.textContent=point
              infowincontent.appendChild(dir);

              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: icon.label
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
              });
            });
          });


         


          var centerControlDiv = document.createElement('div');
          var centerControl = new CenterControl(centerControlDiv, map);

          centerControlDiv.index = 1;
          map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);

        }


          $("#r1").click(function(){

                initMapdir('../objetos/objetosxmlruta.php?ruta=R1DU');

          });

          $("#r2").click(function(){

                initMapdir('../objetos/objetosxmlruta.php?ruta=R2DU');

          });

          $("#r3").click(function(){

                initMapdir('../objetos/objetosxmlruta.php?ruta=R3DU');

          });

          $("#r4").click(function(){

                initMapdir('../objetos/objetosxmlruta.php?ruta=R4DU');

          });

          $("#r5").click(function(){

                initMap();

          });



        function initMapdir(clave) {

	         var formubicacion=$("#formubicacion");	

	 
             //var posini1=new google.maps.LatLng(15.349018,-90.870659);
	          map = new google.maps.Map(document.getElementById('map'), {
	          center: posini,
	          zoom: 16,
	          mapTypeId: 'satellite'

             });
            
            var pos="(15.349018,-90.870659)";
		    var marker = new google.maps.Marker({
		      	
	          position:posini,
	          map: map,
	          title:"CREDICONFINS"+pos,
	          visible:true
	        });


	        google.maps.event.addListener(map,"click",function(event){
        	
        	var coordenadas=event.latLng.toString();

        	coordenadas=coordenadas.replace("(","");
        	coordenadas=coordenadas.replace(")","");

        	var lista=coordenadas.split(",");

        	var direccion= new google.maps.LatLng(lista[0],lista[1]);

        	 marcador = new google.maps.Marker({
        		position: direccion, //La posicion del nuevo marcador
        		map:map,// en que mapa se ubicara el marcador
        		animation:google.maps.Animation.DROP,// como aparecera el marcador
        		draggable:false//no permitir el arrastre del marcador
        	});

        	 //Pasar las coordenadas al formulario
        	 formubicacion.find("input[name='cx']").val(lista[0]);
        	 formubicacion.find("input[name='cy']").val(lista[1]);

        	 //Dejar solo 1 marcador en el mapa
        	 //Guradar el marcador en el array

        	 marcadores_nuevos.push(marcador);

        	 google.maps.event.addListener(marcador,"click",function(){
        	 	
        	 });

        	//Antes de ubicar el marcador en el mapa, quitar todos los demas y asi solo dejar 1
        	quitar_marcadores(marcadores_nuevos);
        	marcador.setMap(map);
        });
        

          var infoWindow = new google.maps.InfoWindow;

          downloadUrl(clave,function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var name = markerElem.getAttribute('name');
              var ape=markerElem.getAttribute('apellido');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name+' '+ape
              infowincontent.appendChild(strong);
               infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              infowincontent.appendChild(document.createElement('br'));
              
              var dir=document.createElement('text');
              dir.textContent=point
              infowincontent.appendChild(dir);

              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: icon.label
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
              });
            });
          });

          var centerControlDiv = document.createElement('div');
          var centerControl = new CenterControl(centerControlDiv, map);

          centerControlDiv.index = 1;
          map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);

        }

         

        


      function downloadUrl(url,callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET',url, true);

        request.send(null);
      }

      function doNothing() {}

    </script>

     <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqwtP7iOSFJg9nr-Rujny6muCJTVPxgaw&callback=initMap">
    </script>

  <script>
      $.extend( true, $.fn.dataTable.defaults, {
          "ordering": false,
          "info":     false,
          "paging":false,
          "lengthMenu": [[5, 5, 5, -1], [5, 5, 5]]
         } );

    $(document).on("ready", function(){
      listar();
      guardar();
      initMap();
      
    });

   $("#btn_listar").on("click", function(){
			listar();
		});


   var guardar = function(){
			$("form").on("submit", function(e){
				e.preventDefault();
				var frm = $(this).serialize();
				$.ajax({
					method: "POST",
					 encoding:"UTF-8",
					url: "../objetos/cobros.php",
					data: frm
				}).done( function( info ){
				console.log( info );		
					var json_info = JSON.parse( info );
					mostrar_mensaje( json_info );
					limpiar_datos();
					listar();
					initMap();
				    
				});
			});
		}



		var mostrar_mensaje = function( informacion ){
			var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
					texto = "<strong>OPERACION REALIZADA CON EXITO!</strong>";
					color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
					texto = "<strong>Error</strong>, no se ejecutó la consulta.";
					color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
					texto = "<strong>Información!</strong> el usuario ya existe.";
					color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
					texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
					color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
					texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
					color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
					$(this).html("");
					$(this).fadeIn(3000);
			});			
		}

		var limpiar_datos = function(){
			$("#opcion").val("registrar");
			$("#identificacion").val("");
			$("#cx").val("");
			$("#cy").val("");
		}

    var listar = function(){
    	$("#cuadro2").slideUp("slow");
		$("#cuadro1").slideDown("slow");
      var table = $("#dt_cliente").DataTable({
        
        "destroy":true,

        "ajax":{
          "method":"POST",
          "url": "../objetos/ficheroActualizarUbicacion.php"
          },
        "columns":[
          {"data":"nombre"},
          {"data":"apellido"},
          {"defaultContent": "<button type='button' class='editar btn btn-warning btn-xs'>Ubicación</button>"}  
        ],
        "language": idioma_espanol
      });

      obtener_data_editar("#dt_cliente tbody", table);
      table.search( 'Por nombre' ).draw();

      
    }
    
    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
         var identificacion = $("#identificacion").val( data.identificacion),
	         codigo=$("#codigo").val(data.identificacion),
			 nombre = $("#nombre").val( data.nombre),
			 apellido = $("#apellido").val( data.apellido),
			 opcion = $("#opcion").val("registrarubicacion");
        $("#cuadro2").slideDown("slow");
		$("#cuadro1").slideUp("slow");

      });
    }


   
    var idioma_espanol = {
        "sZeroRecords":    "No se encontraron resultados",
        "sSearch":         "Buscar:"
    }
    
  </script>
     
</body>
</html>
