<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">  
<link rel="stylesheet" type="text/css" href="../view/cssDT/dataTables.bootstrap.min.css">
<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">
  <!-- Buttons DataTables -->
  
  <title>Usuarios</title>
</head>

<body id="pag">
   
           <header>
              <img src="logo.gif">
              <?php
                echo "<b>Usuario</b>: ".$_SESSION["usuario"];
              ?>
           </header>

           <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Usuarios del sistema</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="cliente.php">Clientes</a>
                    <a class="dropdown-item" href="empleado.php">Personal</a>
                    <a class="dropdown-item" href="usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../objetos/clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="usuarioindividual.php">Usuarios</a>
                     <a class="dropdown-item" href="recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="../objetos/clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="../objetos/revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="../objetos/actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>

   <div id="espacio"></div>
   <div class="container">
   <div class="row">
    <div id="cuadro2" class="col-sm-4 col-md-4 col-lg-4 ocultar">
      <div id="margenform" class="border border-info">
      <form class="form-horizontal" action="" method="POST">
        <input type="hidden" id="identificacion" name="identificacion" value="0">
        <input type="hidden" id="opcion" name="opcion" value="registrar">
        <div class="form-group">
          <label for="usuario" class="col-sm-3 control-label">Usuario: </label>
          <div class="col-sm-9"><input id="usuario" name="usuario" type="text" class="form-control"></div>
        </div>
        <div class="form-group">
          <label for="password" class="col-sm-3 control-label">Password: </label>
          <div class="col-sm-9"><input id="password" name="password" type="text" class="form-control" required></div>
        </div>
        <div class="form-group">
          <label for="privilegio" class="col-sm-3 control-label">Privilegio: </label>
          <div class="col-sm-9"><input id="privilegio" name="privilegio" type="text" class="form-control"></div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-0 col-sm-12">
            <input id="" type="submit" class="btn btn-success btn-md btn-block" value="Actualizar">
            
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-0 col-sm-12">
            <input id="btn_listar" type="button" class="btn btn-info btn-md btn-block" value="Listar">
          </div>
        </div>
      </form>
    </div>
      <div class="col-sm-offset-2 col-sm-8">
        <p class="mensaje"></p>
      </div>
    </div>
  </div> 

  </div> 

  <div class="container">
      <div class="row row justify-content-md-center">
    <div id="cuadro1" class="col-sm-12 col-md-10 col-lg-10">
      <div class="col-sm-offset-2 col-sm-8">
        <h3 class="text-center"> <small class="mensaje"></small></h3>
      </div>
      <div class="table-responsive col-sm-12">    
        <table id="dt_cliente" class="table table-bordered table-hover table-condensed stripe" cellspacing="0" width="100%">
          <thead bgcolor="#58ACFA">
            <tr>
                <th >NOMBRE</th>
                <th>APELLIDO</th>
                <th>RUTA</th>
                <th>USUARIO</th>
                <th>PRIVILEGIO</th>
                <th></th>
                <th colspan="2">Acciones</th>                     
            </tr>
          </thead>          
        </table>
      </div>      
    </div>    
  </div>

  </div>      
   
  

  <div>
    <form id="frmEliminarUsuario" action="" method="POST">
      <input type="hidden" id="idusuario" name="idusuario" value="">
      <input type="hidden" id="opcion" name="opcion" value="eliminar">
      <!-- Modal -->
      <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modalEliminarLabel">Eliminar Usuario</h4>
            </div>
            <div class="modal-body">              
              ¿Está seguro de eliminar al usuario?<strong data-name=""></strong>
            </div>
            <div class="modal-footer">
              <button type="button" id="eliminar-usuario" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
    </form>
  </div>


     <script src="../view/js/jquery-3.2.1.min.js"></script>
     <script src="../view/js/popper.min.js"></script>
     <script src="../view/js/bootstrap.min.js"></script>
     <script src="../view/js/jquery-1.12.3.js"></script>
    
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

  <script>
      $.extend( true, $.fn.dataTable.defaults, {
          "ordering": false,
          "paging":false,
          "info":false
         } );

    $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

    $("#btn_listar").on("click", function(){
      listar();
    });
    
    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "../objetos/cobros.php",
          data: frm
        }).done( function( info ){
        console.log( info );    
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          listar();
        });
      });
    } 

    var eliminar = function(){
      $("#eliminar-usuario").on("click", function(){
        var idusuario = $("#frmEliminarUsuario #idusuario").val(),
          opcion = $("#frmEliminarUsuario #opcion").val();
        $.ajax({
          method:"POST",
          url: "../objetos/cobros.php",
          data: {"idusuario": idusuario, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          listar();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      var texto = "", color = "";
      if( informacion.respuesta == "BIEN" ){
          texto = "<strong>OPERACION REALIZADA CON EXITO!</strong>";
          color = "#379911";
      }else if( informacion.respuesta == "ERROR"){
          texto = "<strong>Error</strong>, no se ejecutó la consulta.";
          color = "#C9302C";
      }else if( informacion.respuesta == "EXISTE" ){
          texto = "<strong>Información!</strong> el usuario ya existe.";
          color = "#5b94c5";
      }else if( informacion.respuesta == "VACIO" ){
          texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
          color = "#ddb11d";
      }else if( informacion.respuesta == "OPCION_VACIA" ){
          texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
          color = "#ddb11d";
      }

      $(".mensaje").html( texto ).css({"color": color });
      $(".mensaje").fadeOut(5000, function(){
          $(this).html("");
          $(this).fadeIn(3000);
      });     
    }

    var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idcredito").val("");
      $("#cliente").val("").focus();
      $("#cantidad").val("");
    }

    var listar = function(){
      $("#cuadro2").slideUp("slow");
      $("#cuadro1").slideDown("slow");
      var table = $("#dt_cliente").DataTable({
        
        "destroy":true,

        "ajax":{
          "method":"POST",
          "url": "../objetos/datos1usuario12.php"
          },
        "columns":[
          {"data":"nombre"},
          {"data":"apellido"},
          {"data":"id_ruta"},
          {"data":"usuario"},
          {"data":"privilegio"},
          {"defaultContent": "<button type='button' class='editar btn btn-warning btn-xs'>Actualizar</button><button type='button' class='eliminar btn btn-danger btn-xs'  data-toggle='modal' data-target='#modalEliminar'>Eliminar</button>"}  
        ],
        "language": idioma_espanol
      });

      obtener_data_editar("#dt_cliente tbody", table);
      obtener_id_eliminar("#dt_cliente tbody", table);
      
    }

    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var identificacion = $("#identificacion").val( data.identificacion ),
            usuario = $("#usuario").val( data.usuario),
            privilegio = $("#privilegio").val( data.privilegio),
            opcion = $("#opcion").val("actualizapass");

            $("#cuadro2").slideDown("slow");
            $("#cuadro1").slideUp("slow");
      });
    }

    var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idusuario = $("#frmEliminarUsuario #idusuario").val( data.identificacion);
      });
    }
   
    var idioma_espanol = {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
    
  </script>        
</body>	
</html>