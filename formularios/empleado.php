<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0 initial-scale=1" />
<link rel="stylesheet" type="text/css" href="../view/css/bootstrap.min.css">  
<link href="../view/css/estilopagina.css" rel="stylesheet" type="text/css">

<title>Registro de personal</title>
</head>

<body id="pag">
     <?php
         require_once "../clases/tipocredito.php"; 
         $tCredito = new TipoCredito();
      ?>
	   <header>
			<img src="logo.gif">
		    <?php
		        echo "<b>Usuario</b>: ".$_SESSION["usuario"];
		    ?>
	    </header>


	    <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1">Registro de personal</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

              <ul class="navbar-nav ml-auto float-lg-right">
                <li class="nav-item">
                  <a class="nav-link" href="../principal.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Registros
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="pagoporruta.php">Pagos</a>
                    <a class="dropdown-item" href="cliente.php">Clientes</a>
                    <a class="dropdown-item" href="empleado.php">Personal</a>
                    <a class="dropdown-item" href="usuario.php">Varios</a>

                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Consultas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../objetos/clientesgeneral45i.php">Clientes</a>
                    <a class="dropdown-item" href="clienteindividual.php">Perfiles</a>
                    <a class="dropdown-item" href="usuarioindividual.php">Usuarios</a>
                     <a class="dropdown-item" href="recordcliente.php">Record cliente</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reportes
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="../reportes/plantillareporte.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/clientesadelantados.php">Adelantados</a>
                    <a class="dropdown-item" href="../objetos/clientesenmora.php">En mora</a>
                    <a class="dropdown-item" href="consultarcobrosporfecha.php">Cobros y colocacion</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actualizaciones
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="mod1Credito3.php">Créditos</a>
                    <a class="dropdown-item" href="../objetos/revocacionTransaccion.php">Transacción</a>
                    <a class="dropdown-item" href="../objetos/actualizacliente.php">Clientes</a>
                    <a class="dropdown-item" href="../objetos/actualizamora.php">Mora y adelantado</a>
                    <a class="dropdown-item" href="ubicaciongeograficacliente.php">Ubicación geográfica</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="cerrar.php">Salir</a>
                </li>
              </ul>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../principal.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Personal</li>
          </ol>
        </nav>

		<div id="espacio"></div>	    
        <div class="container">
        	<div class="row">
        		<div class="col-lg-6">
        	        <div id="margenform" class="border border-info">

				     <form role="form" action="../objetos/empleado.php" method="post">
				          <div class="form-group row">
					         <label for="identificacion" class="col-sm-3 col-form-label">Identificación:</label>
					         <div class="col-sm-9">
					            <input type="text" name="identificacion" id="identificacion" required autofocus="autofocus" class="form-control form-control-sm">
					         </div>   
					      </div>

					      <div class="form-group row">
					      	  <label for="nombre" class="col-sm-3 col-form-label">Nombre:</label>
					      	  <div class="col-sm-9">
					      	    <input type="text" name="nombre" id="nombre" required class="form-control form-control-sm">
					      	  </div>
					     
					      </div>

					      <div class="form-group row">
					      	  <label for="apellido" class="col-sm-3 col-form-label">Apellido:</label>
                              <div class="col-sm-9">
					      	    <input type="text" name="apellido" id="apellido" required class="form-control form-control-sm">
					      	</div>
					      </div>  
					         
					       <div class="form-group row">   
					            <label for="direccion" class="col-sm-3 col-form-label">Dirección:</label>
					            <div class="col-sm-9">
					              <textarea name="direccion" rows="2" cols="30" maxlength="80" required class="form-control form-control-sm"></textarea>
					             </div>
					       </div> 

					       <div class="form-group row">
					       	   <label for="cargo" class="col-sm-3 col-form-label">Cargo:</label>
					       	   <div class="col-sm-9">
					       	      <input type="text" name="cargo" id="cargo" required class="form-control form-control-sm">
                               </div>
					       </div>   	
					             
					        <div class="form-group row">
					         <label for="ruta" class="col-sm-3 col-form-label">Ruta:</label>
					         <div class="col-sm-9">
					          <select name="ruta" required class="form-control form-control-sm">
					                                     <?php
					                                          $array_ruta=$tCredito->consultarRuta();

						                                      foreach($array_ruta as $elemento){
						                                         echo"<option>";
						                                         echo $elemento['id_ruta'];
						                                         echo"</option>";
					                                          }
				                                         ?>
					                                </select>
					          </div>                  
					   
					        </div> 


					        <div class="form-group row">
					             <label for="telefono" class="col-sm-3 col-form-label">Teléfono:</label>
					             <div class="col-sm-9">
					              <input type="text" name="telefono" id="telefono" required class="form-control form-control-sm">
					             </div>
					        </div>

					       <div class="form-group">
					            <input type="submit" name="registrar" value="Registrar" class="btn btn-outline-success btn-md btn-block"> 
					       </div> 
					       <div class="form-group">
					       	   <input type="reset" name="registrar" value="Cancelar" class="btn btn-outline-danger btn-md btn-block">
					       </div>  
				 

				     </form>
				 </div>
			    </div> 

			    <div class="col-lg-5">


			    	<div class="card">
					  <h5 class="card-header text-center">Usuarios del sistema</h5>
					  <div class="card-body">
					       <form role="form" action="../objetos/usuario.php" method="post">
			              <div class="form-group row">
			              	 <label for="usuario" class="col-sm-4 col-form-label">Usuario:</label>
			              	 <div class="col-sm-8">
			              	   <input type="text" name="usuario" id="usuario" required autofocus="autofocus" class="form-control form-control-sm">
                             </div>
			              </div>

			              <div class="form-group row">
			              	 <label for="pass" class="col-sm-4 col-form-label">Password:</label>
			              	 <div class="col-sm-8">
			              	   <input type="text" name="pass" id="pass" required class="form-control form-control-sm">
			              	 </div>
			              </div>

			              <div class="form-group row">
			              	  <label for="privilegio" class="col-sm-4 col-form-label">Privilegio:</label>
			              	  <div class="col-sm-8">
			              	  	 <input type="text" name="privilegio" id="privilegio" required class="form-control form-control-sm">
			              	  </div>
			              	   
			              </div>

			              <div class="form-group row">
			              	  <label for="id_emple" class="col-sm-4 col-form-label">Id. empleado:</label>
			              	  <div class="col-sm-8">
			              	  	<input type="text" name="id_emple" id="id_emple" required class="form-control form-control-sm">
			              	  </div>
			              	   
			              </div>
				      	          
				      	  <div class="form-group">
				      	  	 <input type="submit" name="registrar" value="Registrar" class="btn btn-outline-success btn-md btn-block">
				      	  </div>  
				      	  <div class="form-group">
				      	  	 <input type="reset" name="registrar" value="Cancelar" class="btn btn-outline-danger
				      	  	 btn-md btn-block">
				      	  </div>  
				      	    	           
			           </form>

					  </div>
					</div>
	           
			           
			    </div>
		   </div>	     
	    </div>		     
            

			    

			

			     <footer>
			     </footer>
	 <script src="../view/js/jquery-3.2.1.min.js"></script>
     <script src="../view/js/popper.min.js"></script>
     <script src="../view/js/bootstrap.min.js"></script>     
</body>	
</html>