<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="../estilopagina.css" rel="stylesheet" type="text/css">
<link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../Bootstrap/css/bootstrap-theme.min.css">

<title>Reportes</title>
</head>

<body>
     <div id="contenedor">
           <header>
              <h1>Ver perfil de clientes</h1>
               <h3>CREDICONFINS</h3>
              <?php
                echo "Buen dia: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>

           <nav>
                <ul id="menuHorizontal">
                     <li><a href="../principal.php">Inicio</a>

                     </li>

                     <li>Registros
                         <ul class="submenu">
                             <li><a href="cliente.php">Clientes</a></li>
                             <li><a href="empleado.php">Empleados</a></li>
                             <li><a href="usuario.php">Usuarios</a></li>
                             <li><a href="tipopago.php">Tipos de pago</a></li>
                             <li><a href="sueldo.php">Sueldo</a></li>
                             <li><a href="ruta.php">Rutas</a></li>

                         </ul>
                     </li>

                     <li>Consultas
                           <ul class="submenu">
                             <li><a href="../objetos/clientesgeneral45i.php">Clientes</a></li>
                         </ul>   
                     </li>

                     <li>Reportes
                           <ul class="submenu">
                             <li><a href="reportes/plantillareporte.php">Clientes</a></li>
                         </ul>   
                     </li>
                  
                     <li>Actualizaciones
                           <ul class="submenu">
                             <li><a href="mod1Credito3.php">Créditos</a></li>
                         </ul>     
                     </li>

                     <li><a href="../cerrar.php">Salir</a> 
                              
                     </li>
                
                </ul>
           </nav>
           <br><br>

           <form class="navbar-form navbar-left" role="Search" action="../objetos/perfilcliente.php" method="post">
                <div class="form-group">
                <label>Ingrese identificacion:</label>
                <input type="text" class="form-control" placeholder="Identificacion" name="identificacion" require>
                </div>
                <button type="submit" class="btn btn-default" name="registrar" value="Ver perfil">Ver perfil</button>
           </form>
            
            <form class="navbar-form navbar-left" role="search">
                  <div class="form-group">
                       <input type="text" class="form-control" placeholder="Search">
                   </div>
                    <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <p class="navbar-text navbar-right">Signed in as <a href="#" class="navbar-link">Mark Otto</a></p>



            <ul class="list-group">
  <li class="list-group-item list-group-item-success">Dapibus ac facilisis in</li>
  <li class="list-group-item list-group-item-info">Cras sit amet nibh libero</li>
  <li class="list-group-item list-group-item-warning">Porta ac consectetur ac</li>
  <li class="list-group-item list-group-item-danger">Vestibulum at eros</li>
</ul>
<div class="list-group">
  <a href="#" class="list-group-item list-group-item-success">Dapibus ac facilisis in</a>
  <a href="#" class="list-group-item list-group-item-info">Cras sit amet nibh libero</a>
  <a href="#" class="list-group-item list-group-item-warning">Porta ac consectetur ac</a>
  <a href="#" class="list-group-item list-group-item-danger">Vestibulum at eros</a>
</div>

<div class="list-group">
  <a href="../reportes/reporteGestor.php" class="list-group-item active">
    Cras justo odio
  </a>
   <form role="form" action="../objetos/ruta.php" method="post" class="list-group-item">
              
                      <label>Id. ruta:</label><input type="text" name="id_ruta" id="id_ruta" required class="form-control">
                  <label>Localizacion:</label><input type="text" name="localizacion" id="localizacion" required class="form-control">
                   <input type="submit" name="registrar" value="Registrar" class="btn btn-default"></td>
           </form>
  <a href="#" class="list-group-item">Morbi leo risus</a>
  <form class="navbar-form navbar-left" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Search">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
  <button type="button" class="btn btn-default navbar-btn">Sign in</button>
  <a href="#" class="list-group-item">Vestibulum at eros</a>
</div>


<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img alt="Brand" src="...">
      </a>
    </div>
  </div>
</nav>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img alt="Brand" src="...">
      </a>
    </div>
  </div>
</nav>


<div class="btn-group">
  <button type="button" class="btn btn-danger">Action</button>
  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>


<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>


<ul class="dropdown-menu" aria-labelledby="dropdownMenu4">
  <li><a href="#">Regular link</a></li>
  <li class="disabled"><a href="#">Disabled link</a></li>
  <li><a href="#">Another link</a></li>
</ul>


<ul class="dropdown-menu" aria-labelledby="dropdownMenuDivider">
  ...
  <li role="separator" class="divider"></li>
  ...
</ul>



<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    Dropdown
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
           <footer>
           	
           </footer>
    </div>       
      <script src="../Bootstrap/js/jquery-3.2.1.min.js"></script>  
      <script src="../Bootstrap/js/bootstrap.min.js"></script>  
       <script src="../Bootstrap/js/npm.min.js"></script> 
       
</body>	
</html>


