<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>          
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Clientes activos</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<style type="text/css">

input{
border: none;
}
</style> 
		<link href="../estilopagina.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="../js/clientesactivos.js"></script>
	</head>
	<body>

		<header>
			<h1>Listado general de clientes</h1>
              <img src="logo.gif">
              
		</header>
		 <nav>
                <ul id="menuHorizontal">
                     <li><a href="../principal.php">Inicio</a>

                     </li>

                     <li>Registros
                         <ul class="submenu">
                             <li><a href="pagoporruta.php">Pagos</a></li>
                             <li><a href="cliente.php">Clientes</a></li>
                             <li><a href="empleado.php">Empleados</a></li>
                             <li><a href="usuario.php">Varios</a></li>

                         </ul>
                     </li>

                     <li>Consultas
                         <ul class="submenu">
                             <li><a href="../objetos/clientesgeneral45i.php">Clientes</a></li>
                             <li><a href="clienteindividual.php">Perfiles</a></li>
                              <li><a href="usuarioindividual.php">Usuarios</a></li>
                         </ul>    
                            
                     </li>

                     <li>Reportes
                            <ul class="submenu">
                             <li><a href="../reportes/plantillareporte.php">Clientes</a></li>
                         </ul>  
                     </li>
                  
                     <li>Actualizaciones
                           <ul class="submenu">
                             <li><a href="formularios/mod1Credito3.php">Créditos</a></li>
                         </ul>     
                     </li>

                     <li><a href="../cerrar.php">Salir</a> 
                              
                     </li>
                
                </ul>
           	

           </nav>

		<section id="buscador">
			<input type="text" name="busqueda" id="busqueda" placeholder="Buscar..." class="form-control" autofocus="autofocus">
		</section>

		<section id="tabla_resultado">
                
        </section>

	</body>
</html>

