<?php session_start();if(!isset($_SESSION["usuario"])){header("Location:../index.html");}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="../estilopagina.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../objetos/css/bootstrap.min.css">
  <link rel="stylesheet" href="../objetos/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="../objetos/css/estilos.css">
  <!-- Buttons DataTables -->
  <link rel="stylesheet" href="../objetos/css/buttons.bootstrap.min.css">
  <link rel="stylesheet" href="../objetos/css/font-awesome.min.css">
  <title>Perfiles</title>
</head>

<body>
    <div id="contenedor"> 

           <header>
              <h1>Pefil de clientes</h1>
              <img src="logo.gif">
              <?php
                echo "Usuario: ".$_SESSION["usuario"]."<br><br>";
              ?>
           </header>

           <nav>
                <ul id="menuHorizontal">
                     <li><a href="../principal.php">Inicio</a>

                     </li>

                     <li>Registros
                         <ul class="submenu">
                             <li><a href="pagoporruta.php">Pagos</a></li>

                         </ul>
                     </li>

                     <li>Consultas
                         <ul class="submenu">
                             <li><a href="../objetos/clientesgeneral45i.php">Clientes</a></li>
                         </ul>    
                            
                     </li>

                     <li>Reportes
                            <ul class="submenu">
                             <li><a href="../reportes/plantillareporte.php">Clientes</a></li>
                         </ul>  
                     </li>
                  
                     <li>Actualizaciones
                           <ul class="submenu">
                             <li><a href="mod1Credito3.php">Créditos</a></li>
                         </ul>     
                     </li>

                     <li><a href="../cerrar.php">Salir</a> 
                              
                     </li>
                
                </ul>
           	

           </nav>
   
  <div class="row">
    <div id="cuadro1" class="col-sm-12 col-md-12 col-lg-12">
      <div class="col-sm-offset-2 col-sm-8">
        <h3 class="text-center"> <small class="mensaje"></small></h3>
      </div>
      <div class="table-responsive col-sm-12">    
        <table id="dt_cliente" class="table table-bordered table-hover table-condensed stripe" cellspacing="0" width="100%">
          <thead bgcolor="#58ACFA">
            <tr>
                <th >Id. pago</th>
                <th style="width: 200px;">Nombre</th>
                <th style="width: 200px;">Apellido</th>
                <th>Ruta</th>
                <th>Viaticos</th>
                <th>Sueldo base</th>
                <th>Recaduo</th>
                <th>Porcentaje</th>
                <th>Sueldo total</th>
                 <th>Fecha</th>
                <th colspan="2">Acciones</th>                     
            </tr>
          </thead>          
        </table>
      </div>      
    </div>    
  </div>
  
  <script src="../objetos/js/jquery-1.12.3.js"></script>
  <script src="../objetos/js/bootstrap.min.js"></script>
  <script src="../objetos/js/jquery.dataTables.min.js"></script>
  <script src="../objetos/js/dataTables.bootstrap.js"></script>
  <!--botones DataTables--> 
  <script src="../objetos/js/dataTables.buttons.min.js"></script>
  <script src="../objetos/js/buttons.bootstrap.min.js"></script>

  <script>
      $.extend( true, $.fn.dataTable.defaults, {
          "ordering": false
         } );

    $(document).on("ready", function(){
      listar();
      guardar();
    });

    $("#btn_listar").on("click", function(){
      listar();
    });


    var mostrar_mensaje = function( informacion ){
      var texto = "", color = "";
      if( informacion.respuesta == "BIEN" ){
          texto = "<strong>OPERACION REALIZADA CON EXITO!</strong>";
          color = "#379911";
      }else if( informacion.respuesta == "ERROR"){
          texto = "<strong>Error</strong>, no se ejecutó la consulta.";
          color = "#C9302C";
      }else if( informacion.respuesta == "EXISTE" ){
          texto = "<strong>Información!</strong> el usuario ya existe.";
          color = "#5b94c5";
      }else if( informacion.respuesta == "VACIO" ){
          texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
          color = "#ddb11d";
      }else if( informacion.respuesta == "OPCION_VACIA" ){
          texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
          color = "#ddb11d";
      }

      $(".mensaje").html( texto ).css({"color": color });
      $(".mensaje").fadeOut(5000, function(){
          $(this).html("");
          $(this).fadeIn(3000);
      });     
    }

    var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idcredito").val("");
      $("#cliente").val("").focus();
      $("#cantidad").val("");
    }

    var listar = function(){
      var table = $("#dt_cliente").DataTable({
        
        "destroy":true,

        "ajax":{
          "method":"POST",
          "url": "../objetos/sueldopagado14p.php"
          },
        "columns":[
          {"data":"id_pago"},
          {"data":"nombre"},
          {"data":"apellido"},
          {"data":"id_ruta"},
          {"data":"viaticos"},
          {"data":"sueldo_base"},
          {"data":"recaudo"},
          {"data":"porcentaje"},
          {"data":"sueldo_total"},
          {"data":"fecha"},
          {"defaultContent": "<button type='button' class='editar btn btn-warning btn-xs'>VER</button>"}  
        ],
        "language": idioma_espanol
      });

      obtener_data_editar("#dt_cliente tbody", table);
      
    }

    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var data = table.row( $(this).parents("tr") ).data();
        var idusuario = $("#frmEliminarUsuario #id").val(data.identificacion);
            idruta = $("#frmEliminarUsuario2 #idusuario2").val(data.id_pago);
      });
    }
   
    var idioma_espanol = {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
    
  </script>


  <div id="c1">
  <div id="f1">
  <form  id="frmEliminarUsuario" action="../objetos/perfilemple.php" method="post">
    <div class="col-xs-5">
    <input id="id" name="id" type="text" class="form-control " required >
    </div>
    <input type="submit" name="" value="PERFIL" class="btn btn-warning btn-sm">
  </form>
   </div>
   </div>
   <div id="c2">
   <div id="f2">
  <form  id="frmEliminarUsuario2" action="../reportes/recibopago.php" method="post" target="blank">
    <input id="idusuario2" name="id" type="hidden" required>
    <input type="submit" name="" value="Generar recibo" class="btn btn-info btn-sm">
  </form>
  </div>

  </div>            
</body>	
</html>